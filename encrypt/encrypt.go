package encrypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
)

func Encrypt(key, text []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	b := base64.StdEncoding.EncodeToString(text)
	ciphertext := make([]byte, aes.BlockSize+len(b))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, err
	}
	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(ciphertext[aes.BlockSize:], []byte(b))
	return ciphertext, nil
}

func EncodeHex(key, text []byte) string {
	ciphertext, err := Encrypt(key, text)
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%x", ciphertext)
}

func DecodeHexString(cp string) []byte {
	data, err := hex.DecodeString(cp)
	if err != nil {
		panic(err)
	}
	return data
}

func Decrypt(key, text []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	if len(text) < aes.BlockSize {
		return nil, errors.New("ciphertext too short")
	}
	iv := text[:aes.BlockSize]
	text = text[aes.BlockSize:]
	cfb := cipher.NewCFBDecrypter(block, iv)
	cfb.XORKeyStream(text, text)
	data, err := base64.StdEncoding.DecodeString(string(text))
	if err != nil {
		return nil, err
	}
	return data, nil
}

func DecryptPlainText(key, text []byte) string {
	result, err := Decrypt(key, text)
	if err != nil {
		log.Fatal(err)
	}
	return string(result)
}

func KeyDragonGetENV(name string, isDecode bool) string {
	if isDecode {
		return DecryptPlainText([]byte(os.Getenv("DRAGON_SLAYER")), DecodeHexString(os.Getenv(name)))
	}
	return os.Getenv(name)
}

func EncryptMapString(payload map[string]string) map[string]string {
	k := []byte(os.Getenv("DRAGON_SLAYER"))
	for key, element := range payload {
		payload[key] = EncodeHex(k, []byte(element))
	}
	return payload
}

func DecryptMapString(payload map[string]string) map[string]string {
	k := []byte(os.Getenv("DRAGON_SLAYER"))
	for key, element := range payload {
		payload[key] = DecryptPlainText(k, DecodeHexString(element))
	}
	return payload
}
