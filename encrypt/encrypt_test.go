package encrypt

import (
	"fmt"
	"testing"
)

func TestEncrypt(t *testing.T) {
	key := []byte("yaxCSPGTHJC4jvBfCQ4Q4xJWbL6We4UJ")
	plaintext := []byte(`{
    "a" : "token_a",
    "b" : "token_b",
    "message" : "xxx"
}`)

	cp := EncodeHex(key, plaintext)

	data := DecodeHexString(cp)

	fmt.Println(cp)

	result := DecryptPlainText(key, data)

	fmt.Println(result)
}
