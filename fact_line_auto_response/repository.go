package fact_line_auto_response

import (
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
	yellowMongo "yellow-sdk/mongodb"
)


type Repository interface {
	List(payload *IRequestListPagination) (error, []map[string]interface{})
	ListGraph(payload *IRequestListPagination) (error, []map[string]interface{})
	ListExport(payload *IRequestListPagination) (error, []map[string]interface{})
	ListExportDetail(payload *IRequestListPagination) (error, []map[string]interface{})
	ListTotal(payload *IRequestListPagination) (error, []map[string]interface{})
	Show(_id primitive.ObjectID) (error, *ITable)
	CountDuplicateByName(name string) (error, int32)
	Create(payload *ITable) (error, string, interface{})
	Import(payload []interface{}) (error, string, interface{})
	Update(_id primitive.ObjectID, payload *ITable) (error, string, interface{})
	Delete(_id primitive.ObjectID) (error, string, interface{})
	ToggleActive(_id primitive.ObjectID, payload *ITable) (error, string, interface{})
	ToggleActiveOther(_id primitive.ObjectID) (error, string, interface{})
	AppUpdate(_id primitive.ObjectID, payload *ITable) (error, string, interface{})
}

type repository struct {
	CollectionName string
	DB             *mongo.Database
	Collection     *yellowMongo.MongoCollection
}

func NewRepository(db *mongo.Database) Repository {
	return &repository{
		CollectionName: CollectionName,
		DB:             db,
		Collection:     yellowMongo.NewCollection(db, CollectionName),
	}
}

func (d *repository) getListFilterGraph(payload *IRequestListPagination) []bson.M {
	filter := bson.D{}
	//filter = append(filter, bson.E{Key: "deleted_at", Value: nil})
	if payload.Filter.Type != "" {
		filter = append(filter, bson.E{
			Key:   "type",
			Value: payload.Filter.Type,
		})
	}
	if payload.Filter.Mid != "" {
		filter = append(filter, bson.E{
			Key:   "mid",
			Value: payload.Filter.Mid,
		})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}

	pipeline := []bson.M{
		{"$match": filter},
		//{
		//	"$project": bson.M{
		//		"date":  bson.M{"$dateToString":  bson.M{"format": "%d/%m/%Y", "date": "$created_at"}},
		//		"date2":  bson.M{"$dateToString":  bson.M{"format": "%Y-%m-%d", "date": "$created_at"}},
		//	},
		//},
		{
			 //$group: {_id: {"date": "$date", date2: "$date2"}, count: {$sum: 1}}
			"$group": bson.M{
				//"_id":  bson.M{"date": "$date", "date2": "$date2"},
				"_id":  bson.M{ "$dateToString":  bson.M{ "format": "%Y-%m-%d", "date": "$created_at" } },
				//"created_at":  bson.M{ "$dateToString":  bson.M{ "format": "%Y-%m-%d", "date": "$created_at" } },
				"count": bson.M{"$sum": 1},
				//"date": "$date",
			},
		},
		{
			"$sort": bson.M{"_id": 1},
		},
	}

	//fmt.Println(pipeline)
	return pipeline
}

func (d *repository) getListFilter(payload *IRequestListPagination) []bson.M {
	filter := bson.D{}
	//filter = append(filter, bson.E{Key: "deleted_at", Value: nil})
	if payload.Filter.Type != "" {
		filter = append(filter, bson.E{
			Key:   "type",
			Value: payload.Filter.Type,
		})
	}
	if payload.Filter.Mid != "" {
		filter = append(filter, bson.E{
			Key:   "mid",
			Value: payload.Filter.Mid,
		})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}

	pipeline := []bson.M{
		{"$match": filter},
		{
			"$group": bson.M{
				"_id":   "$ref_title" ,
				"count": bson.M{"$sum": 1},
			},
		},

	}
	pipeline = append(
		pipeline,
		bson.M{"$limit": payload.Limit},
		bson.M{"$skip": payload.Offset},
	)

	//fmt.Println(pipeline)
	return pipeline
}

func (d *repository) getListFilterTotal(payload *IRequestListPagination) []bson.M {
	filter := bson.D{}
	//filter = append(filter, bson.E{Key: "deleted_at", Value: nil})
	if payload.Filter.Type != "" {
		filter = append(filter, bson.E{
			Key:   "type",
			Value: payload.Filter.Type,
		})
	}
	if payload.Filter.Mid != "" {
		filter = append(filter, bson.E{
			Key:   "mid",
			Value: payload.Filter.Mid,
		})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}

	pipeline := []bson.M{
		{"$match": filter},
		{
			"$group": bson.M{
				"_id":   "$ref_title" ,
				"count": bson.M{"$sum": 1},
			},
		},

	}
	//pipeline = append(
	//	pipeline,
	//	bson.M{"$limit": payload.Limit},
	//	bson.M{"$skip": payload.Offset},
	//)

	//fmt.Println(pipeline)
	return pipeline
}

func (d *repository) getListFilterExport(payload *IRequestListPagination) []bson.M {
	filter := bson.D{}
	//filter = append(filter, bson.E{Key: "deleted_at", Value: nil})
	if payload.Filter.Type != "" {
		filter = append(filter, bson.E{
			Key:   "type",
			Value: payload.Filter.Type,
		})
	}
	//if payload.Filter.Mid != "" {
	//	filter = append(filter, bson.E{
	//		Key:   "mid",
	//		Value: payload.Filter.Mid,
	//	})
	//}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}

	pipeline := []bson.M{
		{"$match": filter},
		{
			"$group": bson.M{
				"_id":   "$ref_title" ,
				"count": bson.M{"$sum": 1},
			},
		},

	}
	//pipeline = append(
	//	pipeline,
	//	bson.M{"$limit": payload.Limit},
	//	bson.M{"$skip": payload.Offset},
	//)

	//fmt.Println(pipeline)
	return pipeline
}

func (d *repository) getListFilterExportDetail(payload *IRequestListPagination) []bson.M {
	filter := bson.D{}
	//filter = append(filter, bson.E{Key: "deleted_at", Value: nil})
	if payload.Filter.Type != "" {
		filter = append(filter, bson.E{
			Key:   "type",
			Value: payload.Filter.Type,
		})
	}
	//if payload.Filter.Mid != "" {
	//	filter = append(filter, bson.E{
	//		Key:   "mid",
	//		Value: payload.Filter.Mid,
	//	})
	//}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}

	pipeline := []bson.M{
		{"$match": filter},
		//{
		//	"$group": bson.M{
		//		"_id":   "$mid" ,
		//		"count": bson.M{"$ref_title": 1},
		//	},
		//},

	}
	//pipeline = append(
	//	pipeline,
	//	bson.M{"$limit": payload.Limit},
	//	bson.M{"$skip": payload.Offset},
	//)

	//fmt.Println(pipeline)
	return pipeline
}

func (d *repository) ListGraph(payload *IRequestListPagination) (error, []map[string]interface{}) {
	orderBy := -1
	sort := "created_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}

	findOptions := options.Find()
	findOptions.SetSort(&bson.D{{sort, orderBy}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	filter := d.getListFilterGraph(payload)
	fmt.Println("\n filter ",filter)

	//Find Execute
	rows := make([]map[string]interface{}, 0)
	//rows := make([]*types.IResponseChatHistoryListUserPagination, 0)
	yellowMongo.MongoExecutorFindWithPipeline(d.DB.Collection(d.CollectionName), filter,
		func(cur *mongo.Cursor) {
			item := cur.Current
			var m map[string]interface{}
			if err := bson.Unmarshal(item, &m); err != nil {
				panic(err)
			}

			rows = append(rows, m)
		})
	return nil, rows
}

func (d *repository) List(payload *IRequestListPagination) (error, []map[string]interface{}) {
	orderBy := -1
	sort := "created_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}

	findOptions := options.Find()
	findOptions.SetSort(&bson.D{{sort, orderBy}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	filter := d.getListFilter(payload)
	fmt.Println("\n filter ",filter)

	//Find Execute
	rows := make([]map[string]interface{}, 0)
	//rows := make([]*types.IResponseChatHistoryListUserPagination, 0)
	yellowMongo.MongoExecutorFindWithPipeline(d.DB.Collection(d.CollectionName), filter,
		func(cur *mongo.Cursor) {
			item := cur.Current
			fmt.Println("item ",item)
			var m map[string]interface{}
			if err := bson.Unmarshal(item, &m); err != nil {
				panic(err)
			}

			rows = append(rows, m)
		})
	return nil, rows
}

func (d *repository) ListTotal(payload *IRequestListPagination) (error, []map[string]interface{}) {
	orderBy := -1
	sort := "created_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}

	findOptions := options.Find()
	findOptions.SetSort(&bson.D{{sort, orderBy}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	filter := d.getListFilterTotal(payload)
	fmt.Println("\n filter ",filter)

	//Find Execute
	rows := make([]map[string]interface{}, 0)
	//rows := make([]*types.IResponseChatHistoryListUserPagination, 0)
	yellowMongo.MongoExecutorFindWithPipeline(d.DB.Collection(d.CollectionName), filter,
		func(cur *mongo.Cursor) {
			item := cur.Current
			fmt.Println("item ",item)
			var m map[string]interface{}
			if err := bson.Unmarshal(item, &m); err != nil {
				panic(err)
			}

			rows = append(rows, m)
		})
	return nil, rows
}

func (d *repository) ListExport(payload *IRequestListPagination) (error, []map[string]interface{}) {
	orderBy := -1
	sort := "created_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}

	findOptions := options.Find()
	findOptions.SetSort(&bson.D{{sort, orderBy}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	filter := d.getListFilterExport(payload)
	fmt.Println("\n filter ",filter)

	//Find Execute
	rows := make([]map[string]interface{}, 0)
	//rows := make([]*types.IResponseChatHistoryListUserPagination, 0)
	yellowMongo.MongoExecutorFindWithPipeline(d.DB.Collection(d.CollectionName), filter,
		func(cur *mongo.Cursor) {
			item := cur.Current
			fmt.Println("item ",item)
			var m map[string]interface{}
			if err := bson.Unmarshal(item, &m); err != nil {
				panic(err)
			}

			rows = append(rows, m)
		})
	return nil, rows
}

func (d *repository) ListExportDetail(payload *IRequestListPagination) (error, []map[string]interface{}) {
	orderBy := -1
	sort := "created_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}

	findOptions := options.Find()
	findOptions.SetSort(&bson.D{{sort, orderBy}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	filter := d.getListFilterExportDetail(payload)
	fmt.Println("\n filter ",filter)

	//Find Execute
	rows := make([]map[string]interface{}, 0)
	//rows := make([]*types.IResponseChatHistoryListUserPagination, 0)
	yellowMongo.MongoExecutorFindWithPipeline(d.DB.Collection(d.CollectionName), filter,
		func(cur *mongo.Cursor) {
			item := cur.Current
			fmt.Println("item ",item)
			var m map[string]interface{}
			if err := bson.Unmarshal(item, &m); err != nil {
				panic(err)
			}

			rows = append(rows, m)
		})
	return nil, rows
}

func (d *repository) Show(_id primitive.ObjectID) (error, *ITable) {
	var item *ITable
	err := d.Collection.FindOne(bson.D{{"_id", _id}}).Decode(&item)
	if err != nil {
		if err.Error() != "mongo: no documents in result" {
			return err, item
		}
		return nil, nil
	}
	return nil, item
}

func (d *repository) CountDuplicateByName(name string) (error, int32) {
	return d.Collection.CountDocuments(bson.D{{"name", name}})
}

func (d *repository) Create(payload *ITable) (error, string, interface{}) {
	return d.Collection.InsertOne(payload)
}

func (d *repository) Import(payload []interface{}) (error, string, interface{}) {
	return d.Collection.InsertMany(payload)
}

func (d *repository) Update(_id primitive.ObjectID, payload *ITable) (error, string, interface{}) {
	filter := bson.D{{"_id", _id}}
	s := bson.D{
		//{"name", payload.Name},
		//{"items", payload.Items},
		//{"file_name", payload.Filename},
		{"updated_at", payload.UpdatedAt},
	}
	update := bson.D{{"$set", s}}
	return d.Collection.UpdateOne(_id, filter, update)
}

func (d *repository) Delete(_id primitive.ObjectID) (error, string, interface{}) {
	filter := bson.D{{"_id", _id}}
	s := bson.D{
		{"deleted_at", time.Now()},
	}
	update := bson.D{{"$set", s}}
	return d.Collection.UpdateOne(_id, filter, update)
}

func (d *repository) ToggleActive(_id primitive.ObjectID, payload *ITable) (error, string, interface{}) {
	filter := bson.D{{"_id", _id}}
	s := bson.D{
		{"updated_at", payload.UpdatedAt},
	}
	update := bson.D{{"$set", s}}
	return d.Collection.UpdateOne(_id, filter, update)
}

func (d *repository) ToggleActiveOther(_id primitive.ObjectID) (error, string, interface{}) {
	filter := bson.D{}
	filter = append(filter, bson.E{
		Key:   "_id",
		Value: bson.D{{"$ne", _id}},
	})
	s := bson.D{
		{"is_active", false},
	}
	update := bson.D{{"$set", s}}
	return d.Collection.UpdateMany(_id, filter, update)
}

func (d *repository) AppUpdate(_id primitive.ObjectID, payload *ITable) (error, string, interface{}) {
	filter := bson.D{{"_id", _id}}
	s := bson.D{
		//{"name", payload.Name},
	}
	update := bson.D{{"$set", s}}
	return d.Collection.UpdateOne(_id, filter, update)
}

func (d *repository) ListUserQuery(payload *IRequestListPagination, isCount bool, isRaw bool) []bson.M {
	//order := -1
	//sort := "last_message_at"
	//if payload.Sort != "" {
	//	sort = payload.Sort
	//}
	//if payload.Order == "desc" {
	//	order = -1
	//}
	//filter := bson.M{
	//	"deleted_at": nil,
	//	// "baby": nil,
	//}
	//
	//pipeline := []bson.M{
	//	{
	//		"$group": bson.M{
	//			"_id":   "",
	//			"total": bson.M{"$sum": "$quantity"},
	//		},
	//	},
	//}
	//pipeline = append(
	//	pipeline,
	//	bson.M{"$sort": bson.M{sort: order}},
	//	bson.M{"$skip": payload.Offset},
	//	bson.M{"$limit": payload.Limit},
	//)
	//result := []bson.M{}
	//err = db.C("product").Pipe(pipeline).All(&result)
	//return result[0]["total"].(float64), nil

	filter := bson.M{
		"deleted_at": nil,
		// "baby": nil,
	}

	pipeline := []bson.M{
		{"$match": filter},
		{
			"$project": bson.M{
				"date":  bson.M{"$dateToString":  bson.M{"format": "%d/%m/%Y", "date": "$created_at"}},
				"date2":  bson.M{"$dateToString":  bson.M{"format": "%Y-%m-%d", "date": "$created_at"}},
			},

		},
		{
			// $group: {_id: {"date": "$date", date2: "$date2"}, count: {$sum: 1}}
			"$group": bson.M{
				"_id":             bson.M{"date": "$date", "date2": "$date2"},
				"count": bson.M{"$sum": 1},
			},
		},
	}


	return pipeline
}
