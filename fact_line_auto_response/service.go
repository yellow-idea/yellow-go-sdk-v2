package fact_line_auto_response

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	ListGraph(payload *IRequestListPagination) (error, []map[string]interface{}, int32)
	List(payload *IRequestListPagination) (error, []map[string]interface{}, int)
	ListExport(payload *IRequestListPagination) (error, []map[string]interface{}, int32)
	ListExportDetail(payload *IRequestListPagination) (error, []map[string]interface{}, int32)
	Show(id string) (error, *ITable)
	Create(payload *ITable) (error, string, interface{})
	Import(payload []*ITable) (error, string, interface{})
	Update(id string, payload *ITable) (error, string, interface{})
	Delete(id string) (error, string, interface{})
	ToggleActive(id string, payload *ITable) (error, string, interface{})
	AppUpdate(id string, payload *ITable) (error, string, interface{})
}

type service struct {
	Repository Repository
}

func NewService(db *mongo.Database) Service {
	return &service{
		Repository: NewRepository(db),
	}
}

func (d *service) List(payload *IRequestListPagination) (error, []map[string]interface{}, int) {
	// region List
	err1, rows := d.Repository.List(payload)
	if err1 != nil {
		return err1, nil, 0
	}
	// endregion

	// region Total
	err2, total := d.Repository.ListTotal(payload)
	if err2 != nil {
		return err2, nil, 0
	}
	//// endregion

	return nil, rows, len(total)
}

func (d *service) ListGraph(payload *IRequestListPagination) (error, []map[string]interface{}, int32) {
	// region List
	err1, rows := d.Repository.ListGraph(payload)
	if err1 != nil {
		return err1, nil, 0
	}
	// endregion

	// region Total
	//err2, total := d.Repository.ListTotal(payload)
	//if err2 != nil {
	//	return err2, nil, 0
	//}
	//// endregion

	return nil, rows, 0
}

func (d *service) ListExport(payload *IRequestListPagination) (error, []map[string]interface{}, int32) {
	// region List
	err1, rows := d.Repository.ListExport(payload)
	if err1 != nil {
		return err1, nil, 0
	}
	// endregion

	// region Total
	//err2, total := d.Repository.ListTotal(payload)
	//if err2 != nil {
	//	return err2, nil, 0
	//}
	//// endregion

	return nil, rows, 0
}

func (d *service) ListExportDetail(payload *IRequestListPagination) (error, []map[string]interface{}, int32) {
	// region List
	err1, rows := d.Repository.ListExportDetail(payload)
	if err1 != nil {
		return err1, nil, 0
	}
	// endregion

	// region Total
	//err2, total := d.Repository.ListTotal(payload)
	//if err2 != nil {
	//	return err2, nil, 0
	//}
	//// endregion

	return nil, rows, 0
}

func (d *service) Show(id string) (error, *ITable) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, nil
	}
	// endregion

	return d.Repository.Show(_id)
}

func (d *service) Create(payload *ITable) (error, string, interface{}) {
	payload.ID = primitive.NewObjectID()
	payload.CreatedAt = time.Now()
	payload.UpdatedAt = payload.CreatedAt

	err, message, newID := d.Repository.Create(payload)

	return err, message, newID
}

func (d *service) Import(payload []*ITable) (error, string, interface{}) {
	var data []interface{}
	for _, item := range payload {
		item.ID = primitive.NewObjectID()
		item.CreatedAt = time.Now()
		item.UpdatedAt = item.CreatedAt
		data = append(data, item)
	}
	return d.Repository.Import(data)
}

func (d *service) Update(id string, payload *ITable) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	payload.UpdatedAt = time.Now()

	return d.Repository.Update(_id, payload)
}

func (d *service) Delete(id string) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	return d.Repository.Delete(_id)
}

func (d *service) ToggleActive(id string, payload *ITable) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	return d.Repository.ToggleActive(_id, payload)
}

func (d *service) AppUpdate(id string, payload *ITable) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	payload.UpdatedAt = time.Now()

	return d.Repository.Update(_id, payload)
}
