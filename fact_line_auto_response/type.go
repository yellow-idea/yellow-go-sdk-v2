package fact_line_auto_response

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)


type ITable struct {
	ID        primitive.ObjectID `json:"id" bson:"_id"`
	Payload      []*map[string]interface{}             `json:"payload" bson:"payload"`
	Mid     	string     `json:"mid" bson:"mid"`
	Action      string             `json:"action" bson:"action"`
	Type      string             `json:"type" bson:"type"`
	Token      string             `json:"token" bson:"token"`
	Ref      string             `json:"ref" bson:"ref"`
	RefTitle      string             `json:"ref_title" bson:"ref_title"`
	CreatedAt time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time          `json:"updated_at" bson:"updated_at"`
	DeletedAt time.Time          `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
}

type T struct {
	Date  string `json:"date"`
	Date2 string `json:"date2"`
}

type ITableGraph struct {
	Id struct {
		Date  string `json:"date"`
		Date2 string `json:"date2"`
	} `json:"_id"`
	Count struct {
		NumberInt string `json:"$numberInt"`
	} `json:"count"`
}
type IRequestListPaginationFilter struct {
	Mid     	string     `json:"mid" bson:"mid"`
	Type      string             `json:"type" bson:"type"`
	StartDate time.Time `json:"start_date" bson:"start_date"`
	EndDate   time.Time `json:"end_date" bson:"end_date"`
}

type IRequestListPagination struct {
	Sort     string                        `json:"sort" bson:"sort"`
	Order    string                        `json:"order" bson:"order"`
	Offset   int64                         `json:"offset" bson:"offset"`
	Limit    int64                         `json:"limit" bson:"limit"`
	IsExport bool                          `json:"is_export" bson:"is_export"`
	Filter   *IRequestListPaginationFilter `json:"filter" bson:"filter"`
}