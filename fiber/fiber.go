package fiber

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"time"
)

type IFiberLoggerContentCustomFieldsAppLog struct {
	UserName string `json:"user_name"`
	IP       string `json:"ip"`
}

type IFiberLoggerContent struct {
	Timestamp          time.Time                              `json:"timestamp"`
	Level              string                                 `json:"level"`
	Path               string                                 `json:"path"`
	Method             string                                 `json:"method"`
	UserID             string                                 `json:"user_id"`
	StatusCode         int                                    `json:"status_code"`
	Versioning         string                                 `json:"versioning"`
	UserAgent          string                                 `json:"user_agent"`
	CustomFieldsAppLog *IFiberLoggerContentCustomFieldsAppLog `json:"custom_fields_app_log"`
}

type IFiberLogger struct {
	Type    string               `json:"type"`
	Content *IFiberLoggerContent `json:"content"`
}

func BadRequest(c *fiber.Ctx, err error) error {
	return c.Status(fiber.StatusBadRequest).JSON(map[string]interface{}{"status": "BAD_REQUEST", "message": err.Error()})
}

func PageNotFound(c *fiber.Ctx) error {
	return c.Status(fiber.StatusNotFound).JSON(map[string]interface{}{"status": "PAGE_NOT_FOUND"})
}

func InternalServer(c *fiber.Ctx, err error) error {
	return c.Status(fiber.StatusInternalServerError).JSON(map[string]interface{}{"status": "ERROR", "message": err.Error()})
}

func ResponsePagination(c *fiber.Ctx, rows interface{}, total interface{}) error {
	return c.Status(fiber.StatusOK).JSON(map[string]interface{}{"rows": rows, "total": total})
}

func ResponseDetail(c *fiber.Ctx, data interface{}) error {
	return c.Status(fiber.StatusOK).JSON(map[string]interface{}{"data": data})
}

func ResponseSave(c *fiber.Ctx, status interface{}, id interface{}) error {
	return c.Status(fiber.StatusOK).JSON(map[string]interface{}{"status": status, "id": id})
}

func LogApiV1(c *fiber.Ctx) error {
	_ = c.Next()
	level := "info"
	if c.Response().StatusCode() != 200 ||
		c.Response().StatusCode() != 201 {
		level = "error"
	}
	dataLogger := IFiberLogger{
		Type: "APP_LOG",
		Content: &IFiberLoggerContent{
			Timestamp:  time.Now(),
			Level:      level,
			Path:       c.Path(),
			Method:     string(c.Request().Header.Method()),
			UserID:     "-",
			StatusCode: c.Response().StatusCode(),
			Versioning: "1.0.0",
			UserAgent:  string(c.Request().Header.UserAgent()),
			CustomFieldsAppLog: &IFiberLoggerContentCustomFieldsAppLog{
				UserName: "-",
				IP:       c.IP(),
			},
		},
	}
	b, err := json.Marshal(dataLogger)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	fmt.Println(string(b))
	return nil
}
