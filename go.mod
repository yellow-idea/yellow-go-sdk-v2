module yellow-sdk

go 1.15

require (
	github.com/getsentry/sentry-go v0.9.0
	github.com/gin-gonic/gin v1.4.0
	github.com/go-redis/redis/v8 v8.7.1
	github.com/gofiber/fiber/v2 v2.5.0
	github.com/google/uuid v1.2.0
	github.com/line/line-bot-sdk-go v7.8.0+incompatible
	github.com/spf13/viper v1.7.1
	go.mongodb.org/mongo-driver v1.4.6
)
