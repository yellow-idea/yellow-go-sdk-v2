package line_auto_response

import "go.mongodb.org/mongo-driver/bson/primitive"

func getIDStringMock() string {
	return "601b14d067358904b112aa33"
}

func getIDMock() primitive.ObjectID {
	_id, _ := primitive.ObjectIDFromHex(getIDStringMock())
	return _id
}

func getPayloadMock() *ITable {
	return &ITable{
		ID:   primitive.NewObjectID(),
		Name: "Demo-01",
		Messages: []map[string]interface{}{
			{
				"type": "text",
				"text": "Hello 1",
			},
			{
				"type": "text",
				"text": "Hello 2",
			},
		},
		IsActive: true,
	}
}

func getPayloadImportMock() []interface{} {
	data := make([]interface{}, 0)
	for i := 1; i <= 3; i++ {
		data = append(data, &ITable{
			ID:   primitive.NewObjectID(),
			Name: "Demo-01",
			Messages: []map[string]interface{}{
				{
					"type": "text",
					"text": "Hello 1",
				},
				{
					"type": "text",
					"text": "Hello 2",
				},
			},
			IsActive: true,
		})
	}
	return data
}

func getPayloadPaginationMock() *IRequestListPagination {
	return &IRequestListPagination{
		Sort:   "",
		Order:  "",
		Offset: 0,
		Limit:  10,
		Filter: &IRequestListPaginationFilter{
			IsActive: "1",
		},
	}
}
