package line_broadcast

import (
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"testing"
	yellowTester "yellow-sdk/tester"
)

func TestRepositoryCreate(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewRepository(db)
		payload := getPayloadMock()
		err, message, _id := sv.Create(payload)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(_id, message)
	}
	yellowTester.MongoServerlessModulesRepositorySDK(fn)
}

func TestRepositoryImport(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewRepository(db)
		payload := getPayloadImportMock()
		err, message, _id := sv.Import(payload)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(_id, message)
	}
	yellowTester.MongoServerlessModulesRepositorySDK(fn)
}

func TestRepositoryUpdate(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewRepository(db)
		// region Mock
		_id := getIDMock()
		payload := getPayloadMock()
		payload.ID = _id
		payload.Name = "LineBroadcast-Update"
		// endregion
		err, message, id := sv.Update(_id, payload)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(id, message)
	}
	yellowTester.MongoServerlessModulesRepositorySDK(fn)
}

func TestRepositoryList(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewRepository(db)
		payload := getPayloadPaginationMock()
		err, rows := sv.List(payload)
		if err != nil {
			t.Error(err)
		}
		err, total := sv.ListTotal(payload)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(total, rows)
	}
	yellowTester.MongoServerlessModulesRepositorySDK(fn)
}

func TestRepositoryShow(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewRepository(db)
		_id := getIDMock()
		err, data := sv.Show(_id)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(data)
	}
	yellowTester.MongoServerlessModulesRepositorySDK(fn)
}

func TestRepositoryCountDuplicateByName(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewRepository(db)
		err, total := sv.CountDuplicateByName("LineBroadcast-02")
		if err != nil {
			t.Error(err)
		}
		fmt.Println(total)
	}
	yellowTester.MongoServerlessModulesRepositorySDK(fn)
}

func TestRepositoryDelete(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewRepository(db)
		_id := getIDMock()
		err, message, id := sv.Delete(_id)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(id, message)
	}
	yellowTester.MongoServerlessModulesRepositorySDK(fn)
}

func TestRepositoryToggleActive(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewRepository(db)
		_id := getIDMock()
		payload := getPayloadMock()
		payload.IsActive = false
		err, message, id := sv.ToggleActive(_id, payload)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(id, message)
	}
	yellowTester.MongoServerlessModulesRepositorySDK(fn)
}

func TestRepositoryToggleActiveOther(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewRepository(db)
		_id := getIDMock()
		err, message, id := sv.ToggleActiveOther(_id)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(id, message)
	}
	yellowTester.MongoServerlessModulesRepositorySDK(fn)
}