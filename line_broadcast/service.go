package line_broadcast

import (
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	List(payload *IRequestListPagination) (error, []*ITable, int32)
	Show(id string) (error, *ITable)
	Create(payload *ITable) (error, string, interface{})
	Import(payload []*ITable) (error, string, interface{})
	Update(id string, payload *ITable) (error, string, interface{})
	UpdateIsActive(id string, Recipients int) (error, string, interface{})
	UpdateSentRecurring(id string, Recipients int) (error, string, interface{})
	Delete(id string) (error, string)
	ToggleActive(id string, payload *ITable) (error, string, interface{})
	AppUpdate(id string, payload *ITable) (error, string, interface{})
}

type service struct {
	Repository Repository
}

func NewService(db *mongo.Database) Service {
	return &service{
		Repository: NewRepository(db),
	}
}

func (d *service) List(payload *IRequestListPagination) (error, []*ITable, int32) {
	// region List
	err1, rows := d.Repository.List(payload)
	if err1 != nil {
		return err1, nil, 0
	}
	// endregion

	// region Total
	err2, total := d.Repository.ListTotal(payload)
	if err2 != nil {
		return err2, nil, 0
	}
	// endregion

	return nil, rows, total
}

func (d *service) Show(id string) (error, *ITable) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, nil
	}
	// endregion

	return d.Repository.Show(_id)
}

func (d *service) Create(payload *ITable) (error, string, interface{}) {
	fmt.Println("create()")
	payload.ID = primitive.NewObjectID()
	payload.IsActive = true
	payload.CreatedAt = time.Now()
	payload.UpdatedAt = payload.CreatedAt

	if payload.BroadcastTime == "send now" {
		payload.SendDate = time.Now()
		payload.SendTime = "00:00"
		payload.Sent = time.Now()

	} else if payload.BroadcastTime == "schedule" {
		payload.Sent = time.Now()

	} else if payload.BroadcastTime == "re-curring" {
		payload.SendDate = time.Now()
		//payload.SendTime = "00:00"
		now := time.Now()
		after := now.AddDate(0, 0, -1)
		fmt.Println("after ",after)

		payload.Sent = after

	}
	//payload.Sent = time.Now()
	err, message, newID := d.Repository.Create(payload)

	return err, message, newID
}

func (d *service) Import(payload []*ITable) (error, string, interface{}) {
	var data []interface{}
	for _, item := range payload {
		item.ID = primitive.NewObjectID()
		item.IsActive = false
		item.CreatedAt = time.Now()
		item.UpdatedAt = item.CreatedAt
		data = append(data, item)
	}
	return d.Repository.Import(data)
}

func (d *service) Update(id string, payload *ITable) (error, string, interface{}) {
	fmt.Println("Update")
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	payload.UpdatedAt = time.Now()

	return d.Repository.Update(_id, payload)
}

func (d *service) UpdateIsActive(id string, Recipients int) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion
	return d.Repository.UpdateIsActive(_id,Recipients)
}

func (d *service) UpdateSentRecurring(id string, Recipients int) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion
	return d.Repository.UpdateSentRecurring(_id,Recipients)
}

func (d *service) Delete(id string) (error, string) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST"
	}
	// endregion

	return d.Repository.Delete(_id)
}

func (d *service) ToggleActive(id string, payload *ITable) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	return d.Repository.ToggleActive(_id, payload)
}

func (d *service) AppUpdate(id string, payload *ITable) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	payload.UpdatedAt = time.Now()

	return d.Repository.Update(_id, payload)
}
