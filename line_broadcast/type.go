package line_broadcast

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ITable struct {
	ID             primitive.ObjectID       `json:"id" bson:"_id"`
	Name           string                   `json:"name" bson:"name"`
	Type      string       `json:"type" bson:"type"`
	SegmentID      primitive.ObjectID       `json:"segment_id" bson:"segment_id"`
	QuickSegmentID primitive.ObjectID       `json:"quick_segment_id" bson:"quick_segment_id"`
	StartDate      time.Time                `json:"start_date" bson:"start_date"`
	EndDate        time.Time                `json:"end_date" bson:"end_date"`
	BroadcastTime  string                   `json:"broadcast_time" bson:"broadcast_time"`
	SendTest       string                   `json:"send_test" bson:"send_test"`
	ReCurring      string                   `json:"re_curring" bson:"re_curring"`
	ReCurringTime  string                   `json:"re_curring_time" bson:"re_curring_time"`
	SendDate       time.Time            	`json:"send_date" bson:"send_date" default:"2000-01-01T00:00:00.000Z"`
	SendTime       string                   `json:"send_time" bson:"send_time" default:"00:00"`
	Messages       []map[string]interface{}						`json:"messages" bson:"messages"`
	IsActive       bool                     `json:"is_active" bson:"is_active"`
	CreatedAt      time.Time                `json:"created_at" bson:"created_at"`
	UpdatedAt      time.Time                `json:"updated_at" bson:"updated_at"`
	DeletedAt      time.Time                `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
	Sent    	   time.Time            	`json:"sent" bson:"sent" default:"2000-01-01T00:00:00.000Z"`
	Draft       	bool                     `json:"draft" bson:"draft" default:"false"`
	Mid       		string                     `json:"mid" bson:"mid" default:"false"`
	Recipients  	int 					`json:"recipients" bson:"recipients"`

}


type T struct {
	Text               string `json:"text,omitempty"`
	Type               string `json:"type,omitempty"`
	OriginalContentUrl string `json:"originalContentUrl,omitempty"`
	PreviewImageUrl    string `json:"previewImageUrl,omitempty"`
	Actions            []Actions `json:"actions,omitempty"`
	AltText  string `json:"altText,omitempty"`
	BaseSize struct {
		Height int `json:"height,omitempty"`
		Width  int `json:"width,omitempty"`
	} `json:"baseSize,omitempty"`
	BaseUrl string `json:"baseUrl,omitempty"`
	Body    struct {
		PaddingAll string `json:"paddingAll,omitempty"`
		Action     struct {
			Label   string `json:"label,omitempty"`
			Type    string `json:"type,omitempty"`
			Text    string `json:"text,omitempty"`
			LinkUri string `json:"linkUri,omitempty"`
		} `json:"action,omitempty"`
		Type     string `json:"type,omitempty"`
		Layout   string `json:"layout,omitempty"`
		Contents []struct {
			Type     string `json:"type,omitempty"`
			Layout   string `json:"layout,omitempty"`
			Contents []struct {
				Url        string `json:"url,omitempty"`
				Size       string `json:"size,omitempty"`
				Gravity    string `json:"gravity,omitempty"`
				AspectMode string `json:"aspectMode,omitempty"`
				Type       string `json:"type,omitempty"`
				PaddingAll string `json:"paddingAll,omitempty"`
				Flex       int    `json:"flex,omitempty"`
				Position   string `json:"position,omitempty"`
				Spacing    string `json:"spacing,omitempty"`
				Layout     string `json:"layout,omitempty"`
				Contents   []struct {
					Type     string `json:"type,omitempty"`
					Layout   string `json:"layout,omitempty"`
					Contents []struct {
						Type       string `json:"type,omitempty"`
						Url        string `json:"url,omitempty"`
						Size       string `json:"size,omitempty"`
						AspectMode string `json:"aspectMode,omitempty"`
					} `json:"contents,omitempty"`
					Width        string `json:"width,omitempty"`
					Height       string `json:"height,omitempty"`
					CornerRadius string `json:"cornerRadius,omitempty"`
					Color        string `json:"color,omitempty"`
					Align        string `json:"align,omitempty"`
					Gravity      string `json:"gravity,omitempty"`
					Weight       string `json:"weight,omitempty"`
					Style        string `json:"style,omitempty"`
					Text         string `json:"text,omitempty"`
					Size         string `json:"size,omitempty"`
				} `json:"contents,omitempty"`
			} `json:"contents,omitempty"`
		} `json:"contents,omitempty"`
	} `json:"body,omitempty"`
}

type Actions struct {
	Area Area `json:"area,omitempty"`
	Text string `json:"text,omitempty"`
	Type string `json:"type,omitempty"`
}

type Area struct {
	Y      int `json:"y"`
	Width  int `json:"width"`
	Height int `json:"height"`
	X      int `json:"x"`
}
type BaseSize struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}

type T2 struct {
	Type               string `json:"type"`
	Text               string `json:"text"`
	OriginalContentUrl string `json:"originalContentUrl"`
	PreviewImageUrl    string `json:"previewImageUrl"`
	BaseSize  BaseSize          `json:"baseSize"`
	Actions []Actions `json:"actions"`
	BaseUrl  string `json:"baseUrl"`
	AltText  string `json:"altText"`
	Contents struct {
		Type     string `json:"type"`
		Contents []struct {
			Type string `json:"type"`
			Body struct {
				Type     string `json:"type"`
				Layout   string `json:"layout"`
				Contents []struct {
					Type     string `json:"type"`
					Layout   string `json:"layout"`
					Contents []struct {
						Url        string `json:"url"`
						Size       string `json:"size"`
						Gravity    string `json:"gravity"`
						AspectMode string `json:"aspectMode"`
						Type       string `json:"type"`
						Flex       int    `json:"flex"`
						Position   string `json:"position"`
						Spacing    string `json:"spacing"`
						Layout     string `json:"layout"`
						Contents   []struct {
							Contents []struct {
								Type       string `json:"type"`
								Url        string `json:"url"`
								Size       string `json:"size"`
								AspectMode string `json:"aspectMode"`
							} `json:"contents"`
							Width        string `json:"width"`
							Height       string `json:"height"`
							CornerRadius string `json:"cornerRadius"`
							Type         string `json:"type"`
							Layout       string `json:"layout"`
							Size         string `json:"size"`
							Color        string `json:"color"`
							Align        string `json:"align"`
							Gravity      string `json:"gravity"`
							Weight       string `json:"weight"`
							Style        string `json:"style"`
							Text         string `json:"text"`
						} `json:"contents"`
						PaddingAll string `json:"paddingAll"`
						Action     struct {
							Label string `json:"label"`
							Type  string `json:"type"`
							Text  string `json:"text"`
						} `json:"action"`
					} `json:"contents"`
				} `json:"contents"`
				PaddingAll string `json:"paddingAll"`
			} `json:"body"`
		} `json:"contents"`
	} `json:"contents"`
}
type IRequestListPaginationFilter struct {
	Name      string    `json:"name" bson:"name"`
	Type      string    `json:"type" bson:"type"`
	IsActive  string    `json:"is_active" bson:"is_active"`
	StartDate time.Time `json:"start_date" bson:"start_date"`
	EndDate   time.Time `json:"end_date" bson:"end_date"`
}

type IRequestListPagination struct {
	Sort     string                        `json:"sort" bson:"sort"`
	Order    string                        `json:"order" bson:"order"`
	Offset   int64                         `json:"offset" bson:"offset"`
	Limit    int64                         `json:"limit" bson:"limit"`
	IsExport bool                          `json:"is_export" bson:"is_export"`
	Filter   *IRequestListPaginationFilter `json:"filter" bson:"filter"`
}


