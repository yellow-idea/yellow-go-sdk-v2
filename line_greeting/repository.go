package line_greeting

import (
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
	yellowMongo "yellow-sdk/mongodb"
)

type Repository interface {
	List(payload *IRequestListPagination) (error, []*ITable)
	ListTotal(payload *IRequestListPagination) (error, int32)
	Show(_id primitive.ObjectID) (error, *ITable)
	CountDuplicateByName(name string) (error, int32)
	Create(payload *ITable) (error, string, interface{})
	Import(payload []interface{}) (error, string, interface{})
	Update(_id primitive.ObjectID, payload *ITable) (error, string, interface{})
	Delete(_id primitive.ObjectID) (error, string, interface{})
	ToggleActive(_id primitive.ObjectID, payload *ITable) (error, string, interface{})
	ToggleActiveOther(_id primitive.ObjectID) (error, string, interface{})
}

type repository struct {
	CollectionName string
	DB             *mongo.Database
	Collection     *yellowMongo.MongoCollection
}

func NewRepository(db *mongo.Database) Repository {
	return &repository{
		CollectionName: CollectionName,
		DB:             db,
		Collection:     yellowMongo.NewCollection(db, CollectionName),
	}
}

func (d *repository) getListFilter(payload *IRequestListPagination) bson.D {
	filter := bson.D{}
	filter = append(filter, bson.E{Key: "deleted_at", Value: nil})
	if payload.Filter.Name != "" {
		filter = append(filter, bson.E{
			Key:   "name",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.Name)}},
		})
	}
	if payload.Filter.IsActive == "1" {
		filter = append(filter, bson.E{Key: "is_active", Value: true})
	} else if payload.Filter.IsActive == "0" {
		filter = append(filter, bson.E{Key: "is_active", Value: false})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}
	return filter
}

func (d *repository) List(payload *IRequestListPagination) (error, []*ITable) {
	orderBy := -1
	sort := "updated_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}

	findOptions := options.Find()
	findOptions.SetSort(&bson.D{{sort, orderBy}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	filter := d.getListFilter(payload)

	rows := make([]*ITable, 0)
	err := yellowMongo.MongoExecutorFind(d.DB.Collection(d.CollectionName), filter, findOptions,
		func(cur *mongo.Cursor) {
			var item *ITable
			_ = cur.Decode(&item)
			rows = append(rows, item)
		})
	defer func() { rows = make([]*ITable, 0) }()
	return err, rows
}

func (d *repository) ListTotal(payload *IRequestListPagination) (error, int32) {
	return d.Collection.CountDocuments(d.getListFilter(payload))
}

func (d *repository) Show(_id primitive.ObjectID) (error, *ITable) {
	var item *ITable
	err := d.Collection.FindOne(bson.D{{"_id", _id}}).Decode(&item)
	if err != nil {
		if err.Error() != "mongo: no documents in result" {
			return err, item
		}
		return nil, nil
	}
	return nil, item
}

func (d *repository) CountDuplicateByName(name string) (error, int32) {
	return d.Collection.CountDocuments(bson.D{{"name", name}})
}

func (d *repository) Create(payload *ITable) (error, string, interface{}) {
	return d.Collection.InsertOne(payload)
}

func (d *repository) Import(payload []interface{}) (error, string, interface{}) {
	return d.Collection.InsertMany(payload)
}

func (d *repository) Update(_id primitive.ObjectID, payload *ITable) (error, string, interface{}) {
	filter := bson.D{{"_id", _id}}
	s := bson.D{
		{"name", payload.Name},
		{"messages", payload.Messages},
		{"is_active", payload.IsActive},
		{"updated_at", payload.UpdatedAt},
	}
	update := bson.D{{"$set", s}}
	return d.Collection.UpdateOne(_id, filter, update)
}

func (d *repository) Delete(_id primitive.ObjectID) (error, string, interface{}) {
	filter := bson.D{{"_id", _id}}
	s := bson.D{
		{"is_active", false},
		{"deleted_at", time.Now()},
	}
	update := bson.D{{"$set", s}}
	return d.Collection.UpdateOne(_id, filter, update)
}

func (d *repository) ToggleActive(_id primitive.ObjectID, payload *ITable) (error, string, interface{}) {
	filter := bson.D{{"_id", _id}}
	s := bson.D{
		{"is_active", payload.IsActive},
		{"updated_at", payload.UpdatedAt},
	}
	update := bson.D{{"$set", s}}
	return d.Collection.UpdateOne(_id, filter, update)
}

func (d *repository) ToggleActiveOther(_id primitive.ObjectID) (error, string, interface{}) {
	filter := bson.D{}
	filter = append(filter, bson.E{
		Key:   "_id",
		Value: bson.D{{"$ne", _id}},
	})
	s := bson.D{
		{"is_active", false},
	}
	update := bson.D{{"$set", s}}
	return d.Collection.UpdateMany(_id, filter, update)
}
