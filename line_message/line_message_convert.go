package line_message

import (
	"encoding/json"
	"github.com/line/line-bot-sdk-go/linebot"
	"strings"
)

const LineSchemaLocationURL = "https://line.me/R/nv/location"

type rawImageMapMessage struct {
	Type     string `json:"type"`
	BaseURL  string `json:"baseUrl"`
	AltText  string `json:"altText"`
	BaseSize struct {
		Width  int `json:"width"`
		Height int `json:"height"`
	} `json:"baseSize"`
	Actions []struct {
		Type    string `json:"type"`
		Label   string `json:"label"`
		LinkURI string `json:"linkUri,omitempty"`
		Area    struct {
			X      int `json:"x"`
			Y      int `json:"y"`
			Width  int `json:"width"`
			Height int `json:"height"`
		} `json:"area"`
		Text string `json:"text,omitempty"`
	} `json:"actions"`
	Video struct {
		OriginalContentURL string `json:"originalContentUrl"`
		PreviewImageURL    string `json:"previewImageUrl"`
		Area               struct {
			X      int `json:"x"`
			Y      int `json:"y"`
			Width  int `json:"width"`
			Height int `json:"height"`
		} `json:"area"`
	} `json:"video"`
}

func ConvertMapStringToSender(messages []map[string]interface{}, userProfile *linebot.GetProfileCall, altText string) []linebot.SendingMessage {
	var userData *linebot.UserProfileResponse
	message := make([]linebot.SendingMessage, 0)
	fnPersonalize := func(content string) string {
		if content == "" {
			return content
		}
		tagDisplayName := "{Display Name}"
		if userData == nil {
			userData, _ = userProfile.Do()
		}
		if userData != nil {
			i := strings.Index(content, tagDisplayName)
			if i > -1 {
				content = strings.ReplaceAll(content, tagDisplayName, userData.DisplayName)
			}
		}
		return content
	}
	for _, v := range messages {
		messageType := v["type"]
		if messageType != nil {
			if messageType == "text" {
				content := fnPersonalize(v["text"].(string))
				message = append(message, linebot.NewTextMessage(content))
			} else if messageType == "sticker" {
				packageID := v["packageId"].(string)
				stickerID := v["stickerId"].(string)
				message = append(message, linebot.NewStickerMessage(packageID, stickerID))
			} else if messageType == "image" {
				originalContentURL := v["originalContentUrl"].(string)
				previewImageURL := v["previewImageUrl"].(string)
				message = append(message, linebot.NewImageMessage(originalContentURL, previewImageURL))
			} else if messageType == "video" {
				originalContentURL := v["originalContentUrl"].(string)
				previewImageURL := v["previewImageUrl"].(string)
				message = append(message, linebot.NewVideoMessage(originalContentURL, previewImageURL))
			} else if messageType == "audio" {
				originalContentURL := v["originalContentUrl"].(string)
				previewImageURL := v["duration"].(int)
				message = append(message, linebot.NewAudioMessage(originalContentURL, previewImageURL))
			} else if messageType == "location" {
				title := v["title"].(string)
				address := v["address"].(string)
				latitude := v["latitude"].(float64)
				longitude := v["longitude"].(float64)
				message = append(message, linebot.NewLocationMessage(title, address, latitude, longitude))
			} else if messageType == "imagemap" {
				data, _ := json.Marshal(v)
				raw := rawImageMapMessage{}
				if err := json.Unmarshal(data, &raw); err == nil {
					actions := make([]linebot.ImagemapAction, 0)
					for _, v := range raw.Actions {
						area := linebot.ImagemapArea{
							X:      v.Area.X,
							Y:      v.Area.Y,
							Width:  v.Area.Width,
							Height: v.Area.Height,
						}
						if v.Type == "message" {
							actions = append(actions, linebot.NewMessageImagemapAction("", v.Text, area))
						} else if v.Type == "uri" {
							actions = append(actions, linebot.NewURIImagemapAction("", v.LinkURI, area))
						} else if v.Type == "location" {
							actions = append(actions, linebot.NewURIImagemapAction("", LineSchemaLocationURL, area))
						}
					}
					if raw.AltText == "" {
						if altText != "" {
							raw.AltText = altText
						} else {
							raw.AltText = "imagemap"
						}
					}
					message = append(message, linebot.NewImagemapMessage(raw.BaseURL+"?_ignored=", raw.AltText, raw.BaseSize, actions...))
				}
			} else if messageType == "flex" {
				data, _ := json.Marshal(v["contents"])
				flexContainer, err := linebot.UnmarshalFlexMessageJSON(data)
				if err == nil {
					if altText == "" {
						altText = v["altText"].(string)
					}
					message = append(message, linebot.NewFlexMessage(altText, flexContainer))
				}
			} else if messageType == "personalize" {
				userData, _ = userProfile.Do()
				v2 := map[string]interface{}{
					"type":    "flex",
					"altText": "personalize",
					"contents": map[string]interface{}{
						"type": "bubble",
						"body": v["body"],
					},
				}
				data, _ := json.Marshal(v2["contents"])
				jsonString := string(data)
				jsonString = strings.ReplaceAll(jsonString, "Display_Name", userData.DisplayName)
				if userData.PictureURL != "" {
					jsonString = strings.ReplaceAll(jsonString, "https://scdn.line-apps.com/n/channel_devcenter/img/flexsnapshot/clip/clip13.jpg", userData.PictureURL)
				}
				flexContainer, err := linebot.UnmarshalFlexMessageJSON([]byte(jsonString))
				if err == nil {
					if altText == "" {
						altText = "New Message"
					}
					message = append(message, linebot.NewFlexMessage(altText, flexContainer))
				}
			}
		}
	}
	return message
}
