package line_notify

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const urlEndpoint = "https://notify-api.line.me/api/notify"

func SendMessage(token string, projectName string, fn string, e error, data interface{}) {
	j, err := json.Marshal(data)
	if err != nil {
		fmt.Println(err)
	}
	client := &http.Client{
	}
	message := fmt.Sprintf(`
Project  : %s
Function : %s
====================================
Error: %s
Payload: %s`, projectName, fn, e.Error(), j)
	req, err := http.NewRequest("POST", urlEndpoint, strings.NewReader("message="+message))

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Authorization", "Bearer "+token)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()
	_, err = ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
	}
}
