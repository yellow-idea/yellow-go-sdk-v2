package line_rich_menu

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type RichMenuSize struct {
	Width  int `json:"width" bson:"width"`
	Height int `json:"height" bson:"height"`
}

type RichMenuBounds struct {
	X      int `json:"x" bson:"x"`
	Y      int `json:"y" bson:"y"`
	Width  int `json:"width" bson:"width"`
	Height int `json:"height" bson:"height"`
}

type RichMenuAction struct {
	Type    string `json:"type" bson:"type"`
	URI     string `json:"uri,omitempty" bson:"uri"`
	Text    string `json:"text,omitempty" bson:"text"`
	Data    string `json:"data,omitempty" bson:"data"`
	Mode    string `json:"mode,omitempty" bson:"mode"`
	Initial string `json:"initial,omitempty" bson:"initial"`
	Max     string `json:"max,omitempty" bson:"max"`
	Min     string `json:"min,omitempty" bson:"min"`
}

type RichMenuAreaDetail struct {
	Bounds RichMenuBounds `json:"bounds"`
	Action RichMenuAction `json:"action"`
}

type RichMenu struct {
	Size        RichMenuSize         `json:"size" bson:"size"`
	Selected    bool                 `json:"selected" bson:"selected"`
	Name        string               `json:"name" bson:"name"`
	ChatBarText string               `json:"chatBarText" bson:"chatBarText"`
	Areas       []RichMenuAreaDetail `json:"areas" bson:"areas"`
}

type ITable struct {
	ID           primitive.ObjectID `json:"id" bson:"_id"`
	Name         string             `json:"name" bson:"name"`
	SegmentID    primitive.ObjectID `json:"segment_id" bson:"segment_id"`
	QuickSegmentID    primitive.ObjectID `json:"quick_segment_id" bson:"quick_segment_id"`
	Type         string             `json:"type" bson:"type"`
	Keywords     []string           `json:"keywords" bson:"keywords"`
	StartDate    time.Time          `json:"start_date" bson:"start_date"`
	EndDate      time.Time          `json:"end_date" bson:"end_date"`
	MenuBarType  string             `json:"menu_bar_type" bson:"menu_bar_type"`
	MenuBarLabel string             `json:"menu_bar_label" bson:"menu_bar_label"`
	ImageUrl     string             `json:"image_url" bson:"image_url"`
	RichMenu     RichMenu           `json:"rich_menu" bson:"rich_menu"`
	IsActive     bool               `json:"is_active" bson:"is_active"`
	IsUnlink     bool               `json:"is_unlink" bson:"is_unlink"`
	RichMenuID   string             `json:"rich_menu_id" bson:"rich_menu_id"`
	CreatedAt    time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt    time.Time          `json:"updated_at" bson:"updated_at"`
	DeletedAt    time.Time          `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
}

type IRequestListPaginationFilter struct {
	Name      string    `json:"name" bson:"name"`
	IsActive  string    `json:"is_active" bson:"is_active"`
	StartDate time.Time `json:"start_date" bson:"start_date"`
	EndDate   time.Time `json:"end_date" bson:"end_date"`
}

type IRequestListPagination struct {
	Sort     string                        `json:"sort" bson:"sort"`
	Order    string                        `json:"order" bson:"order"`
	Offset   int64                         `json:"offset" bson:"offset"`
	Limit    int64                         `json:"limit" bson:"limit"`
	IsExport bool                          `json:"is_export" bson:"is_export"`
	Filter   *IRequestListPaginationFilter `json:"filter" bson:"filter"`
}