package line_rich_menu_tab

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/line/line-bot-sdk-go/linebot"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

type Service interface {
	List(payload *IRequestListPagination) (error, []*ITable, int32)
	ListForCron(payload *IRequestListPagination) (error, []*ITable, int32)
	Show(id string) (error, *ITable)
	Create(payload *ITable) (error, string, interface{})
	Import(payload []*ITable) (error, string, interface{})
	Update(id string, payload *ITable) (error, string, interface{})
	UpdateforCron(id string) (error, string, interface{})
	Delete(id string) (error, string, interface{})
	ToggleActive(id string, payload *ITable) (error, string, interface{})
	AppUpdate(id string, payload *ITable) (error, string, interface{})
	LinkUserByRichMenuName(lineUserID string, richMenuName string) error
	ForceLinkByUser(lineUserID string, richMenuID string) error
	ForceUnLinkByUser(lineUserID string) error
}

type service struct {
	Repository  Repository
	LineChID    string
	LineChSe    string
	LineChToken string
}

func NewService(db *mongo.Database, lineChID string, lineChSe string, lineChToken string) Service {
	return &service{
		Repository:  NewRepository(db),
		LineChID:    lineChID,
		LineChSe:    lineChSe,
		LineChToken: lineChToken,
	}
}

func (d *service) List(payload *IRequestListPagination) (error, []*ITable, int32) {
	// region List
	err1, rows := d.Repository.List(payload)
	if err1 != nil {
		return err1, nil, 0
	}
	// endregion

	// region Total
	err2, total := d.Repository.ListTotal(payload)
	if err2 != nil {
		return err2, nil, 0
	}
	// endregion

	return nil, rows, total
}

func (d *service) ListForCron(payload *IRequestListPagination) (error, []*ITable, int32) {
	// region List
	err1, rows := d.Repository.ListDelete(payload)
	if err1 != nil {
		return err1, nil, 0
	}
	// endregion

	// region Total
	err2, total := d.Repository.ListTotalDelete(payload)
	if err2 != nil {
		return err2, nil, 0
	}
	// endregion

	return nil, rows, total
}

func (d *service) Show(id string) (error, *ITable) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, nil
	}
	// endregion

	return d.Repository.Show(_id)
}

func (d *service) Create(payload *ITable) (error, string, interface{}) {
	now := time.Now()
	richMenuAliasId1 := "b-"+now.Format("20060102150405")
	richMenuAliasId2 := "a-"+now.Format("20060102150405")
	payload.RichMenuAliasId1 = richMenuAliasId1
	payload.RichMenuAliasId2 = richMenuAliasId2

	for _, a := range payload.RichMenu.Areas {
		if(a.Action.Type == "richmenuswitch"){
			a.Action.RichMenuAliasId = richMenuAliasId1
		}
	}
	for _, a := range payload.RichMenu2.Areas {
		if(a.Action.Type == "richmenuswitch"){
			a.Action.RichMenuAliasId = richMenuAliasId2
		}
	}

	if payload.IsActive {
		// region
		//Create Rich Menu A
		richMenuID, err := d.LineCreateRichMenu(payload.RichMenu)
		if err != nil {
			return err, "CREATE_RICH_MENU_ERROR", nil
		}
		// endregion

		// region Upload Content
		//upload image Rich Menu A
		if payload.ImageUrl != "" {
			err = d.LineUploadRichMenuImage(richMenuID, payload.ImageUrl)
			if err != nil {
				return err, "UPLOAD_CONTENT_RICH_MENU_ERROR", nil
			}
		}
		// endregion

		payload.RichMenuID = richMenuID

		// Create Rich Menu B
		richMenuID2, err := d.LineCreateRichMenu2(payload.RichMenu2)
		if err != nil {
			return err, "CREATE_RICH_MENU_ERROR", nil
		}
		// endregion

		//upload image Rich Menu B
		if payload.ImageUrl2 != "" {
			err = d.LineUploadRichMenuImage(richMenuID2, payload.ImageUrl2)
			if err != nil {
				return err, "UPLOAD_CONTENT_RICH_MENU_ERROR", nil
			}
		}
		// endregion

		payload.RichMenuID2 = richMenuID2

	}
	if payload.RichMenuID != "" {

		p := map[string]interface{}{
			"richMenuAliasId": richMenuAliasId2,
			"richMenuId": payload.RichMenuID,
		}
		err := d.LineCreateRichMenuAlias(p)
		if err != nil {
			return err, "CREATE_RICH_MENU_ALIAS_A_ERROR", nil
		}
	}

	if payload.RichMenuID2 != "" {

		p := map[string]interface{}{
			"richMenuAliasId": richMenuAliasId1,
			"richMenuId": payload.RichMenuID2,
		}
		err := d.LineCreateRichMenuAlias(p)
		if err != nil {
			return err, "CREATE_RICH_MENU_ALIAS_B_ERROR", nil
		}
	}


	payload.ID = primitive.NewObjectID()
	payload.CreatedAt = time.Now()
	payload.UpdatedAt = payload.CreatedAt

	err, message, newID := d.Repository.Create(payload)

	return err, message, newID
}

func (d *service) Import(payload []*ITable) (error, string, interface{}) {
	var data []interface{}
	for _, item := range payload {
		item.ID = primitive.NewObjectID()
		item.IsActive = false
		item.CreatedAt = time.Now()
		item.UpdatedAt = item.CreatedAt
		data = append(data, item)
	}
	return d.Repository.Import(data)
}

func (d *service) Update(id string, payload *ITable) (error, string, interface{}) {
	//region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	payload.UpdatedAt = time.Now()

	// region Get Old Data
	fmt.Println(_id)
	err, oldData := d.Repository.Show(_id)
	fmt.Println(oldData)
	if err != nil {
		return err, "SOMETHING_ERROR", nil
	}
	// endregion

	// region Remove Rich Menu Not Use
	if (oldData.IsActive && payload.IsActive && oldData.RichMenuID != "") ||
		(oldData.IsActive && !payload.IsActive && oldData.RichMenuID != "") {
		// region Clear Old Rich Menu
		err = d.LineDeleteRichMenu(oldData.RichMenuID)
		if err != nil {
			if err.Error() != "HTTP status is 404" {
				return err, "DELETE_RICH_MENU_ERROR", _id
			}
		}
		// endregion
	}
	// endregion

	// region Create Rich Menu If Active
	if (oldData.IsActive && payload.IsActive) || (!oldData.IsActive && payload.IsActive) {
		// TODO: Upload

		now := time.Now()
		richMenuAliasId1 := "b-"+now.Format("20060102150405")
		richMenuAliasId2 := "a-"+now.Format("20060102150405")
		payload.RichMenuAliasId1 = richMenuAliasId1
		payload.RichMenuAliasId2 = richMenuAliasId2

		for _, a := range payload.RichMenu.Areas {
			if(a.Action.Type == "richmenuswitch"){
				a.Action.RichMenuAliasId = richMenuAliasId1
			}
		}
		for _, a := range payload.RichMenu2.Areas {
			if(a.Action.Type == "richmenuswitch"){
				a.Action.RichMenuAliasId = richMenuAliasId2
			}
		}

		if payload.IsActive {
			// region
			//Create Rich Menu A
			richMenuID, err := d.LineCreateRichMenu(payload.RichMenu)
			if err != nil {
				return err, "CREATE_RICH_MENU_ERROR", nil
			}
			// endregion

			// region Upload Content
			//upload image Rich Menu A
			if payload.ImageUrl != "" {
				err = d.LineUploadRichMenuImage(richMenuID, payload.ImageUrl)
				if err != nil {
					return err, "UPLOAD_CONTENT_RICH_MENU_ERROR", nil
				}
			}
			// endregion

			payload.RichMenuID = richMenuID

			// Create Rich Menu B
			richMenuID2, err := d.LineCreateRichMenu2(payload.RichMenu2)
			if err != nil {
				return err, "CREATE_RICH_MENU_ERROR", nil
			}
			// endregion

			//upload image Rich Menu B
			if payload.ImageUrl2 != "" {
				err = d.LineUploadRichMenuImage(richMenuID2, payload.ImageUrl2)
				if err != nil {
					return err, "UPLOAD_CONTENT_RICH_MENU_ERROR", nil
				}
			}
			// endregion

			payload.RichMenuID2 = richMenuID2

		}
		if payload.RichMenuID != "" {

			p := map[string]interface{}{
				"richMenuAliasId": richMenuAliasId2,
				"richMenuId": payload.RichMenuID,
			}
			err := d.LineCreateRichMenuAlias(p)
			if err != nil {
				return err, "CREATE_RICH_MENU_ALIAS_A_ERROR", nil
			}
		}

		if payload.RichMenuID2 != "" {

			p := map[string]interface{}{
				"richMenuAliasId": richMenuAliasId1,
				"richMenuId": payload.RichMenuID2,
			}
			err := d.LineCreateRichMenuAlias(p)
			if err != nil {
				return err, "CREATE_RICH_MENU_ALIAS_B_ERROR", nil
			}
		}

	}
	// endregion

	return d.Repository.Update(_id, payload)
}

func (d *service) UpdateforCron(id string) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	return d.Repository.UpdateforCron(_id)
}

func (d *service) Delete(id string) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	return d.Repository.Delete(_id)
}

func (d *service) ToggleActive(id string, payload *ITable) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	// region Get Old Data
	err, oldData := d.Repository.Show(_id)
	if err != nil {
		return err, "SOMETHING_ERROR", nil
	}
	// endregion

	// region Remove Rich Menu Not Use
	if (oldData.IsActive && !payload.IsActive && oldData.RichMenuID != "") {
		// region Clear Old Rich Menu
		err = d.LineDeleteRichMenu(oldData.RichMenuID)
		if err != nil {
			if err.Error() != "HTTP status is 404" {
				return err, "DELETE_RICH_MENU_ERROR", _id
			}
		}
		// endregion
	}
	// endregion

	// region Create Rich Menu If Active
	if !oldData.IsActive && payload.IsActive {
		// region Create New Rich Menu
		richMenuID, err := d.LineCreateRichMenu(payload.RichMenu)
		if err != nil {
			return err, "CREATE_RICH_MENU_ERROR", nil
		}
		// endregion

		// region Upload Content
		if oldData.ImageUrl != "" {
			err = d.LineUploadRichMenuImage(richMenuID, oldData.ImageUrl)
			if err != nil {
				return err, "UPLOAD_CONTENT_RICH_MENU_ERROR", nil
			}
		}
		// endregion

		payload.RichMenuID = richMenuID
	}
	// endregion

	return d.Repository.ToggleActive(_id, payload)
}

func (d *service) AppUpdate(id string, payload *ITable) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	payload.UpdatedAt = time.Now()

	return d.Repository.Update(_id, payload)
}

type lineCreateRichMenuResponse struct {
	RichMenuID string `json:"richMenuId"`
}


func (d *service) LineCreateRichMenu(richMenu RichMenu) (string, error) {
	j, err := json.Marshal(richMenu)
	if err != nil {
		return "", err
	}
	client := &http.Client{}
	req, err := http.NewRequest("POST", linebot.APIEndpointBase+linebot.APIEndpointCreateRichMenu, bytes.NewBuffer(j))
	if err != nil {
		return "", err
	}
	req.Header.Add("Authorization", "Bearer "+d.LineChToken)
	req.Header.Add("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return "", fmt.Errorf("HTTP status is %d", res.StatusCode)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	var response = new(lineCreateRichMenuResponse)
	err = json.Unmarshal(body, &response)
	if err != nil {
		return "", err
	}
	return response.RichMenuID, nil
}

func (d *service) LineCreateRichMenu2(richMenu RichMenu2) (string, error) {
	j, err := json.Marshal(richMenu)
	if err != nil {
		return "", err
	}
	client := &http.Client{}
	req, err := http.NewRequest("POST", linebot.APIEndpointBase+linebot.APIEndpointCreateRichMenu, bytes.NewBuffer(j))
	if err != nil {
		return "", err
	}
	req.Header.Add("Authorization", "Bearer "+d.LineChToken)
	req.Header.Add("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return "", fmt.Errorf("HTTP status is %d", res.StatusCode)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	var response = new(lineCreateRichMenuResponse)
	err = json.Unmarshal(body, &response)
	if err != nil {
		return "", err
	}
	return response.RichMenuID, nil
}

func (d *service) LineUploadRichMenuImage(richMenuID string, imageURL string) error {
	path, ext, err := LineDownloadTempImage(imageURL)
	if err != nil {
		return err
	}
	binary, err := os.Open(path)
	if err != nil {
		return err
	}
	client := &http.Client{}
	req, err := http.NewRequest("POST", linebot.APIEndpointBaseData+linebot.APIEndpointCreateRichMenu+"/"+richMenuID+"/content", binary)
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+d.LineChToken)
	req.Header.Add("Content-Type", "image/"+ext)
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	err = LineDeleteTempImage(path)
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("HTTP status is %d", res.StatusCode)
	}
	return nil
}

func (d *service) LineDeleteRichMenu(richMenuID string) error {
	client := &http.Client{}
	req, err := http.NewRequest("DELETE", linebot.APIEndpointBase+linebot.APIEndpointCreateRichMenu+"/"+richMenuID, nil)
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+d.LineChToken)
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("HTTP status is %d", res.StatusCode)
	}
	return nil
}

func LineDeleteTempImage(path string) error {
	err := os.Remove(path)
	if err != nil {
		return err
	}
	return nil
}

func LineDownloadTempImage(imageURL string) (string, string, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", imageURL, nil)
	if err != nil {
		return "", "", err
	}
	res, err := client.Do(req)
	if err != nil {
		return "", "", err
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return "", "", fmt.Errorf("HTTP status is %d", res.StatusCode)
	}
	v1, err := uuid.NewUUID()
	if err != nil {
		return "", "", err
	}
	spt := strings.Split(imageURL, ".")
	ext := spt[len(spt)-1]
	if ext == "jpg" {
		ext = "jpeg"
	}
	if ext != "jpeg" && ext != "png" {
		return "", "", fmt.Errorf("Invalid image extension")
	}
	path := "/tmp/" + v1.String() + "." + ext
	file, err := os.Create(path)
	if err != nil {
		return "", "", err
	}
	defer file.Close()
	_, err = io.Copy(file, res.Body)
	if err != nil {
		return "", "", err
	}
	return path, ext, nil
}

func (d *service) LinkUserByRichMenuName(lineUserID string, richMenuName string) error {
	err, data := d.Repository.ShowByName(richMenuName)
	if err != nil {
		return err
	}

	if data.RichMenuID != "" {
		fmt.Println(data.RichMenuID)
		if err := d.ForceLinkByUser(lineUserID, data.RichMenuID); err != nil {
			return err
		}
	}
	return nil
}

func (d *service) ForceLinkByUser(lineUserID string, richMenuID string) error {
	url := fmt.Sprintf("https://api.line.me/v2/bot/user/%s/richmenu/%s", lineUserID, richMenuID)
	method := "POST"
	strPayload := fmt.Sprintf(`{"richMenuId":"%s","userId":"%s"}`, richMenuID, lineUserID)
	payload := strings.NewReader(strPayload)

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+d.LineChToken)
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	_, err = ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusOK {
		fmt.Println(res.StatusCode)
		fmt.Println(url)
		fmt.Println(strPayload)
	}

	return nil
}

func (d *service) ForceUnLinkByUser(lineUserID string) error {
	url := fmt.Sprintf("https://api.line.me/v2/bot/user/%s/richmenu", lineUserID)
	method := "DELETE"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+d.LineChToken)
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	_, err = ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	return nil
}

func (d *service) LineCreateRichMenuAlias(richMenu interface{}) (error) {
	j, err := json.Marshal(richMenu)
	if err != nil {
		return err
	}
	fmt.Println("j ",j)
	client := &http.Client{}
	req, err := http.NewRequest("POST", linebot.APIEndpointBase+"/v2/bot/richmenu/alias", bytes.NewBuffer(j))
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+d.LineChToken)
	req.Header.Add("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(body))
	return nil

}