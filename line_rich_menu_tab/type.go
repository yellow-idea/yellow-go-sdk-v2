package line_rich_menu_tab

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type RichMenuSize struct {
	Width  int `json:"width" bson:"width"`
	Height int `json:"height" bson:"height"`
}

type RichMenuSize2 struct {
	Width  int `json:"width" bson:"width"`
	Height int `json:"height" bson:"height"`
}

type RichMenuBounds struct {
	X      int `json:"x" bson:"x"`
	Y      int `json:"y" bson:"y"`
	Width  int `json:"width" bson:"width"`
	Height int `json:"height" bson:"height"`
}
type RichMenuBounds2 struct {
	X      int `json:"x" bson:"x"`
	Y      int `json:"y" bson:"y"`
	Width  int `json:"width" bson:"width"`
	Height int `json:"height" bson:"height"`
}

type RichMenuAction struct {
	Type    string `json:"type" bson:"type"`
	URI     string `json:"uri,omitempty" bson:"uri"`
	Text    string `json:"text,omitempty" bson:"text"`
	RichMenuAliasId    string `json:"richMenuAliasId,omitempty" bson:"richMenuAliasId"`
	Data    string `json:"data,omitempty" bson:"data"`
	Mode    string `json:"mode,omitempty" bson:"mode"`
	Initial string `json:"initial,omitempty" bson:"initial"`
	Max     string `json:"max,omitempty" bson:"max"`
	Min     string `json:"min,omitempty" bson:"min"`
}

type RichMenuAction2 struct {
	Type    string `json:"type" bson:"type"`
	URI     string `json:"uri,omitempty" bson:"uri"`
	Text    string `json:"text,omitempty" bson:"text"`
	RichMenuAliasId    string `json:"richMenuAliasId,omitempty" bson:"richMenuAliasId"`
	Data    string `json:"data,omitempty" bson:"data"`
	Mode    string `json:"mode,omitempty" bson:"mode"`
	Initial string `json:"initial,omitempty" bson:"initial"`
	Max     string `json:"max,omitempty" bson:"max"`
	Min     string `json:"min,omitempty" bson:"min"`
}

type RichMenuAreaDetail struct {
	Bounds RichMenuBounds `json:"bounds"`
	Action *RichMenuAction `json:"action"`
}
type RichMenuAreaDetail2 struct {
	Bounds RichMenuBounds2 `json:"bounds"`
	Action *RichMenuAction2 `json:"action"`
}

type RichMenu struct {
	Size        RichMenuSize         `json:"size" bson:"size"`
	Selected    bool                 `json:"selected" bson:"selected"`
	Name        string               `json:"name" bson:"name"`
	ChatBarText string               `json:"chatBarText" bson:"chatBarText"`
	Areas       []RichMenuAreaDetail `json:"areas" bson:"areas"`
}

type RichMenu2 struct {
	Size        RichMenuSize2         `json:"size" bson:"size"`
	Selected    bool                 `json:"selected" bson:"selected"`
	Name        string               `json:"name" bson:"name"`
	ChatBarText string               `json:"chatBarText" bson:"chatBarText"`
	Areas       []RichMenuAreaDetail2 `json:"areas" bson:"areas"`
}

type ITable struct {
	ID           primitive.ObjectID `json:"id" bson:"_id"`
	Name         string             `json:"name" bson:"name"`
	SegmentID    primitive.ObjectID `json:"segment_id" bson:"segment_id"`
	QuickSegmentID    primitive.ObjectID `json:"quick_segment_id" bson:"quick_segment_id"`
	Type         string             `json:"type" bson:"type"`
	Keywords     []string           `json:"keywords" bson:"keywords"`
	StartDate    time.Time          `json:"start_date" bson:"start_date"`
	EndDate      time.Time          `json:"end_date" bson:"end_date"`
	MenuBarType  string             `json:"menu_bar_type" bson:"menu_bar_type"`
	MenuBarLabel string             `json:"menu_bar_label" bson:"menu_bar_label"`
	ImageUrl     string             `json:"image_url" bson:"image_url"`
	ImageUrl2     string             `json:"image_url2" bson:"image_url2"`
	RichMenu     RichMenu           `json:"rich_menu" bson:"rich_menu"`
	RichMenu2     RichMenu2           `json:"rich_menu2" bson:"rich_menu2"`
	IsActive     bool               `json:"is_active" bson:"is_active"`
	RichMenuID   string             `json:"rich_menu_id" bson:"rich_menu_id"`
	RichMenuID2   string             `json:"rich_menu_id" bson:"rich_menu_id2"`
	RichMenuAliasId1	string             `json:"rich_menu_alias_id1" bson:"rich_menu_alias_id1"`
	RichMenuAliasId2	string             `json:"rich_menu_alias_id2" bson:"rich_menu_alias_id2"`
	CreatedAt    time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt    time.Time          `json:"updated_at" bson:"updated_at"`
	DeletedAt    time.Time          `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
}

type IRequestListPaginationFilter struct {
	Name      string    `json:"name" bson:"name"`
	IsActive  string    `json:"is_active" bson:"is_active"`
	StartDate time.Time `json:"start_date" bson:"start_date"`
	EndDate   time.Time `json:"end_date" bson:"end_date"`
}

type IRequestListPagination struct {
	Sort     string                        `json:"sort" bson:"sort"`
	Order    string                        `json:"order" bson:"order"`
	Offset   int64                         `json:"offset" bson:"offset"`
	Limit    int64                         `json:"limit" bson:"limit"`
	IsExport bool                          `json:"is_export" bson:"is_export"`
	Filter   *IRequestListPaginationFilter `json:"filter" bson:"filter"`
}