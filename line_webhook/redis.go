package line

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	yellowLineAutoResponse "yellow-sdk/line_auto_response"
	yellowLineGreeting "yellow-sdk/line_greeting"
)

var ctx = context.Background()
var rdb *redis.Client

func GetLineAutoReplyMessage(keyword string, redisHost string, redisPort string, redisPass string, prefix string) []map[string]interface{} {
	fmt.Println("keyword : ", keyword)

	if rdb == nil {
		rdb = redis.NewClient(&redis.Options{
			Addr:     fmt.Sprintf("%s:%s", redisHost, redisPort),
			Password: redisPass,
			DB:       5,
		})
	}

	val, err := rdb.Get(ctx, prefix).Result()
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var data []*yellowLineAutoResponse.ITable
	err = json.Unmarshal([]byte(val), &data)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var result []map[string]interface{}
	var resultDefault []map[string]interface{}
	if len(data) > 0 {
		for _, v := range data {
			if v.UseKeywords {
				for _, _keyword := range v.Keywords {
					if _keyword == keyword {
						result = v.Messages
					}
				}
			} else {
				resultDefault = v.Messages
			}
		}
	}

	if result != nil {
		return result
	}

	return resultDefault
}

func GetLineGreeting(redisHost string, redisPort string, redisPass string, prefix string) []map[string]interface{} {
	if rdb == nil {
		rdb = redis.NewClient(&redis.Options{
			Addr:     fmt.Sprintf("%s:%s", redisHost, redisPort),
			Password: redisPass,
			DB:       5,
		})
	}

	val, err := rdb.Get(ctx, prefix).Result()
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var data *yellowLineGreeting.ITable
	err = json.Unmarshal([]byte(val), &data)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	if data == nil {
		return nil
	}

	return data.Messages
}
