package line

import (
	"fmt"
	"testing"
	yellowTester "yellow-sdk/tester"
)

func TestExampleClient(t *testing.T) {
	yellowTester.GeneralUtilityServerlessCustom("../../../")
	messages := GetLineAutoReplyMessage("test", "", "", "", "")
	fmt.Println(messages)
}
