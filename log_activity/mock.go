package log_activity

import "go.mongodb.org/mongo-driver/bson/primitive"

func getIDStringMock() string {
	return "601b14d067358904b112aa33"
}

func getIDMock() primitive.ObjectID {
	_id, _ := primitive.ObjectIDFromHex(getIDStringMock())
	return _id
}

func getPayloadMock() *ITable {
	return &ITable{
		ID:   primitive.NewObjectID(),
	}
}

func getPayloadImportMock() *ITable {
	return &ITable{
		ID:   primitive.NewObjectID(),
	}
}
