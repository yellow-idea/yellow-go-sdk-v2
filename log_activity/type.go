package log_activity

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ITable struct {
	ID         primitive.ObjectID `json:"id" bson:"_id"`
	ParentID   primitive.ObjectID `json:"parent_id" bson:"parent_id"`
	LineUserID string             `json:"line_user_id" bson:"line_user_id"`
	Email      string             `json:"email" bson:"email"`
	FirstName  string             `json:"first_name" bson:"first_name"`
	LastName   string             `json:"last_name" bson:"last_name"`
	Role       string             `json:"role" bson:"role"`
	Action     string             `json:"action" bson:"action"`
	Data       string             `json:"data" bson:"data"`
	Status     string             `json:"status" bson:"status"`
	CreatedAt  time.Time          `json:"created_at" bson:"created_at"`
}

type IRequestListPaginationFilter struct {
	Admin      string    `json:"admin" bson:"admin"`
	Action      string    `json:"action" bson:"action"`
	Activity  string    `json:"activity" bson:"activity"`
	StartDate time.Time `json:"start_date" bson:"start_date"`
	EndDate   time.Time `json:"end_date" bson:"end_date"`
}

type IRequestListPagination struct {
	Sort     string                        `json:"sort" bson:"sort"`
	Order    string                        `json:"order" bson:"order"`
	Offset   int64                         `json:"offset" bson:"offset"`
	Limit    int64                         `json:"limit" bson:"limit"`
	IsExport bool                          `json:"is_export" bson:"is_export"`
	Filter   *IRequestListPaginationFilter `json:"filter" bson:"filter"`
}


