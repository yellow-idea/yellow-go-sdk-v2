package mongodb

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
	utilSentry "yellow-sdk/sentry"
)

type SecretMongoDetail struct {
	Protocal  string
	IPAddress string
	Username  string
	Password  string
	Options   string
}

type MongoClientType func(SecretMongoDetail, string) (*mongo.Client, *mongo.Database, error)

type MongoCollection struct {
	CollectionName string
	DB             *mongo.Database
}

func NewCollection(db *mongo.Database, collectionName string) *MongoCollection {
	return &MongoCollection{
		CollectionName: collectionName,
		DB:             db,
	}
}

func MongoClient(detail SecretMongoDetail, dbName string) (*mongo.Client, *mongo.Database, error) {
	uri := fmt.Sprintf("%s://%s:%s@%s/?%s", detail.Protocal, detail.Username, detail.Password, detail.IPAddress, detail.Options)
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		return nil, nil, err
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		return nil, nil, err
	}

	return client, client.Database(dbName), nil
}

func (d *MongoCollection) CountDocuments(filter bson.D) (error, int32) {
	total, err := d.DB.Collection(d.CollectionName).CountDocuments(context.TODO(), filter)
	if err != nil {
		return err, 0
	}
	return nil, int32(total)
}

func (d *MongoCollection) FindOne(filter bson.D) *mongo.SingleResult {
	return d.DB.Collection(d.CollectionName).FindOne(context.TODO(), filter)
}

func (d *MongoCollection) InsertOne(payload interface{}) (error, string, interface{}) {
	res, err := d.DB.Collection(d.CollectionName).InsertOne(context.TODO(), payload)
	if err != nil {
		return err, "EXECUTE_ERROR", nil
	}
	return nil, "CREATE_SUCCESS", res.InsertedID
}

func (d *MongoCollection) InsertMany(payload []interface{}) (error, string, interface{}) {
	res, err := d.DB.Collection(d.CollectionName).InsertMany(context.TODO(), payload)
	if err != nil {
		return err, "EXECUTE_ERROR", nil
	}
	return nil, "CREATE_SUCCESS", res.InsertedIDs
}

func (d *MongoCollection) UpdateOne(_id interface{}, filter bson.D, update bson.D) (error, string, interface{}) {
	res, err := d.DB.Collection(d.CollectionName).UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err, "EXECUTE_ERROR", _id
	}
	if res.MatchedCount <= 0 {
		return nil, "DATA_NOT_FOUND", _id
	}
	return nil, "UPDATE_SUCCESS", _id
}

func (d *MongoCollection) UpdateMany(_id interface{}, filter bson.D, update bson.D) (error, string, interface{}) {
	res, err := d.DB.Collection(d.CollectionName).UpdateMany(context.TODO(), filter, update)
	if err != nil {
		return err, "EXECUTE_ERROR", _id
	}
	if res.MatchedCount <= 0 {
		return nil, "DATA_NOT_FOUND", _id
	}
	return nil, "TOGGLE_ACTIVE_SUCCESS", _id
}

func (d *MongoCollection) DeleteOne(filter bson.D) (error, string) {
	res, err := d.DB.Collection(d.CollectionName).DeleteOne(context.TODO(), filter)
	if err != nil {
		return err, "EXECUTE_ERROR"
	}
	if res.DeletedCount <= 0 {
		return nil, "DATA_NOT_FOUND"
	}
	return nil, "DELETE_SUCCESS"
}

func (d *MongoCollection) DeleteMany(filter bson.D) (error, string) {
	res, err := d.DB.Collection(d.CollectionName).DeleteMany(context.TODO(), filter)
	if err != nil {
		return err, "EXECUTE_ERROR"
	}
	if res.DeletedCount <= 0 {
		return nil, "DATA_NOT_FOUND"
	}
	return nil, "DELETE_SUCCESS"
}

func MongoConnection(proto string, host string, user string, pass string, opts string, dbName string) (*mongo.Client, *mongo.Database, error) {
	if proto == "" {
		proto = "mongodb"
	}
	detail := SecretMongoDetail{
		Protocal:  proto,
		IPAddress: host,
		Username:  user,
		Password:  pass,
		Options:   opts,
	}
	client, db, err := MongoClient(detail, dbName)
	return client, db, err
}

func MongoExecutorFind(collection *mongo.Collection,
	filter interface{},
	findOptions *options.FindOptions,
	callback func(cur *mongo.Cursor)) error {
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	if err != nil {
		return err
	}

	for cur.Next(context.TODO()) {
		callback(cur)
	}

	if err := cur.Err(); err != nil {
		return err
	}

	defer func() { _ = cur.Close(context.TODO()) }()

	return nil
}

func MongoCallbackConnect(proto string, host string, user string, pass string, opts string, dbName string, callback func(db *mongo.Database, err error)) {
	client, db, err := MongoConnection(proto, host, user, pass, opts, dbName)
	defer client.Disconnect(context.TODO())
	callback(db, err)
}

func MongoExecutorFindWithPipeline(collection *mongo.Collection, pipeline interface{}, callback func(cur *mongo.Cursor)) {
	//fmt.Println("pipeline ",pipeline)
//	const $data = await fact_auto_reply_and_reply.aggregate(
//	[
//	{$match: filter},
//	{
//		$project: {
//	date: {$dateToString: {format: "%d/%m/%Y", date: "$created_at"}},
//	date2: {$dateToString: {format: "%Y-%m-%d", date: "$created_at"}}
//	}
//	},
//	{
//		$group: {_id: {"date": "$date", date2: "$date2"}, count: {$sum: 1}}
//	},
//	{
//		$sort: {"_id.date2": 1}
//	}
//]
//	);
//	$data.map(v => {
//	result.label.push(v._id.date);
//	result.value.push(v.count);
//	});
//
	cur, err := collection.Aggregate(context.TODO(), pipeline)

	//fmt.Println("mongo cur",cur)

	// Output Error
	if err != nil {
		utilSentry.SendErrorWithPayload(err, &primitive.M{"pipeline": pipeline})
		//LineNotifySendMessage(fn, err, &primitive.M{"pipeline": pipeline})
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {

		callback(cur)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		utilSentry.SendErrorWithPayload(err, &primitive.M{"pipeline": pipeline})
		//LineNotifySendMessage(fn, err, &primitive.M{"pipeline": pipeline})
	}

	defer func() { _ = cur.Close(context.TODO()) }()
}
