package segment

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ITableConditionGroup struct {
	ID          string `json:"id" bson:"id"`
	IsCondition string `json:"is_condition" bson:"is_condition"`
}

type ITableCondition struct {
	ID           string                   `json:"id" bson:"id"`
	ParentID     string                   `json:"parent_id" bson:"parent_id"`
	IsCondition  string                   `json:"is_condition" bson:"is_condition"`
	Title        string                   `json:"title" bson:"title"`
	SubTitle1    string                   `json:"sub_title1" bson:"sub_title1"`
	QuestionList []map[string]interface{} `json:"questionList" bson:"questionList"`
}


type ITableSubCondition struct {
	ID         string                   `json:"id" bson:"id"`
	ParentID   string                   `json:"parent_id" bson:"parent_id"`
	GroupID    string                   `json:"group_id" bson:"group_id"`
	Source1    string                   `json:"source1" bson:"source1"`
	Source2    string                   `json:"source2" bson:"source2"`
	Operator   string                   `json:"operator" bson:"operator"`
	Value1     []string                 `json:"value1" bson:"value1"`
	ValueNumber int                 `json:"valueNumber" bson:"valueNumber"`
	Value2     []string                 `json:"value2" bson:"value2"`
	Layer2     []map[string]interface{} `json:"layers2" bson:"layers2"`       // TODO: ชั่วคราว
	Layer3     []map[string]interface{} `json:"layers3" bson:"layers3"`       // TODO: ชั่วคราว
	ChoiceList []map[string]interface{} `json:"choiceList" bson:"choiceList"` // TODO: ชั่วคราว
}

type ITable struct {
	ID             primitive.ObjectID      `json:"id" bson:"_id"`
	Name           string                  `json:"name" bson:"name"`
	ConditionGroup []*ITableConditionGroup `json:"condition_group" bson:"condition_group"`
	Condition      []*ITableCondition      `json:"condition" bson:"condition"`
	SubCondition   [][]*ITableSubCondition `json:"sub_condition" bson:"sub_condition"`
	IsActive       bool                    `json:"is_active" bson:"is_active"`
	CreatedAt      time.Time               `json:"created_at" bson:"created_at"`
	UpdatedAt      time.Time               `json:"updated_at" bson:"updated_at"`
	DeletedAt      time.Time               `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
}

type IRequestListPaginationFilter struct {
	Name      string    `json:"name" bson:"name"`
	IsActive  string    `json:"is_active" bson:"is_active"`
	StartDate time.Time `json:"start_date" bson:"start_date"`
	EndDate   time.Time `json:"end_date" bson:"end_date"`
}

type IRequestListPagination struct {
	Sort     string                        `json:"sort" bson:"sort"`
	Order    string                        `json:"order" bson:"order"`
	Offset   int64                         `json:"offset" bson:"offset"`
	Limit    int64                         `json:"limit" bson:"limit"`
	IsExport bool                          `json:"is_export" bson:"is_export"`
	Filter   *IRequestListPaginationFilter `json:"filter" bson:"filter"`
}

type IRequestPayload struct {
	ID               string `json:"id" bson:"id"`
	LineUserId       string `json:"line_user_id" bson:"line_user_id"`
	LineDisplayName  string `json:"line_display_name" bson:"line_display_name"`
	LineDisplayImage string `json:"line_display_image" bson:"line_display_image"`
	DataPillar       string `json:"data_pillar" bson:"data_pillar"`
	Definition       string `json:"definition" bson:"definition"`
	DataPoint        string `json:"data_point" bson:"data_point"`
	IsPull           bool   `json:"is_pull" bson:"is_pull"`
	Action           string `json:"action" bson:"action"`
}
