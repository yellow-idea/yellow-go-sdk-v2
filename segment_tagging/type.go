package segment_tagging

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ITableLayersChildren struct {
	ID   string `json:"id" bson:"id"`
	Name string `json:"name" bson:"name"`
}

type ITableLayers struct {
	ID       string                 `json:"id" bson:"id"`
	Name     string                 `json:"name" bson:"name"`
	Children []*ITableLayersChildren `json:"children" bson:"children"`
}

type ITable struct {
	ID        primitive.ObjectID `json:"id" bson:"_id"`
	Name      string             `json:"name" bson:"name"`
	Layers    []*ITableLayers    `json:"layers" bson:"layers"`
	IsActive  bool               `json:"is_active" bson:"is_active"`
	CreatedAt time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time          `json:"updated_at" bson:"updated_at"`
	DeletedAt time.Time          `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
}

type IRequestListPaginationFilter struct {
	Name      string    `json:"name" bson:"name"`
	IsActive  string    `json:"is_active" bson:"is_active"`
	StartDate time.Time `json:"start_date" bson:"start_date"`
	EndDate   time.Time `json:"end_date" bson:"end_date"`
}

type IRequestListPagination struct {
	Sort     string                        `json:"sort" bson:"sort"`
	Order    string                        `json:"order" bson:"order"`
	Offset   int64                         `json:"offset" bson:"offset"`
	Limit    int64                         `json:"limit" bson:"limit"`
	IsExport bool                          `json:"is_export" bson:"is_export"`
	Filter   *IRequestListPaginationFilter `json:"filter" bson:"filter"`
}