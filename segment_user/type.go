package segment_user

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ITableItems struct {
	LINEUserID string `json:"line_user_id" bson:"line_user_id"`
}

type ITable struct {
	ID        primitive.ObjectID `json:"id" bson:"_id"`
	Name      string             `json:"name" bson:"name"`
	Items     []*ITableItems     `json:"items" bson:"items"`
	IsActive  bool               `json:"is_active" bson:"is_active"`
	CreatedAt time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time          `json:"updated_at" bson:"updated_at"`
	DeletedAt time.Time          `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
}

type IRequestListPaginationFilter struct {
	DisplayName      string    `json:"display_name" bson:"display_name"`
	UserId      string    `json:"user_id" bson:"user_id"`
	Status      string    `json:"status" bson:"status"`
	StartDate time.Time `json:"start_date" bson:"start_date"`
	EndDate   time.Time `json:"end_date" bson:"end_date"`
}

type IRequestListPagination struct {
	Sort     string                        `json:"sort" bson:"sort"`
	Order    string                        `json:"order" bson:"order"`
	Offset   int64                         `json:"offset" bson:"offset"`
	Limit    int64                         `json:"limit" bson:"limit"`
	IsExport bool                          `json:"is_export" bson:"is_export"`
	Filter   *IRequestListPaginationFilter `json:"filter" bson:"filter"`
}