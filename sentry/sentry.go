package sentry

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/getsentry/sentry-go"
	"github.com/gin-gonic/gin"
	"time"
)

func Init(dsnName string, env string, serviceName string) {
	err := sentry.Init(sentry.ClientOptions{
		Dsn:         dsnName,
		Environment: env,
		ServerName:  serviceName,
	})
	if err != nil {
		fmt.Printf("sentry.Init: %s", err)
	}
}

func prettyPrint(b []byte) []byte {
	var out bytes.Buffer
	_ = json.Indent(&out, b, "", "  ")
	return out.Bytes()
}

func SendBodyToScope(exception error, payload interface{}, body []byte) {
	sentry.ConfigureScope(func(scope *sentry.Scope) {
		scope.SetRequestBody(body)
		if payload != nil {
			b, err := json.Marshal(payload)
			if err != nil {
				fmt.Println(err)
			}
			scope.AddBreadcrumb(&sentry.Breadcrumb{
				Category: "payload",
				Data: map[string]interface{}{
					"data": string(prettyPrint(b)),
				},
				Level: sentry.LevelInfo,
			}, -1)
		}
		SendErrorWithRouter(exception)
	})
}

func SendErrorWithPayload(exception error, payload interface{}) {
	if payload != nil {
		b, err := json.Marshal(payload)
		if err != nil {
			fmt.Println(err)
		}
		sentry.AddBreadcrumb(&sentry.Breadcrumb{
			Category: "payload",
			Data: map[string]interface{}{
				"data": string(prettyPrint(b)),
			},
			Level: sentry.LevelInfo,
		})
	}
	sentry.CaptureException(exception)
}

func SendErrorWithGin(c *gin.Context, exception error, payload interface{}) {
	sentry.ConfigureScope(func(scope *sentry.Scope) {
		scope.SetRequest(c.Request)
		if payload != nil {
			// TODO:
			//b, err := json.Marshal(payload)
			//if err != nil {
			//	fmt.Println(err)
			//}
			//scope.AddBreadcrumb(&sentry.Breadcrumb{
			//	Category: "payload",
			//	Data: map[string]interface{}{
			//		"data": string(prettyPrint(b)),
			//	},
			//	Level: sentry.LevelInfo,
			//}, -1)
		}
		SendErrorWithRouter(exception)
	})
}

func SendErrorWithRouter(exception error) {
	defer sentry.Flush(2 * time.Second)
	defer sentry.Recover()
	sentry.CaptureException(exception)
}
