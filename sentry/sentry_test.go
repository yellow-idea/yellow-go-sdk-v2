package sentry

import (
	"errors"
	"github.com/getsentry/sentry-go"
	"testing"
	yellowTester "yellow-sdk/tester"
)

func TestSentryDemo(t *testing.T) {
	yellowTester.GeneralUtilityServerless()
	Init("http://be3e3fa86f154b6db9c14ddba8d3ac3d@128.199.93.157:9000/5", "dev", "DemoService")

	sentry.AddBreadcrumb(&sentry.Breadcrumb{
		Category: "auth",
		Message:  "Authenticated user xxx",
		Level:    sentry.LevelInfo,
	})

	sentry.WithScope(func(scope *sentry.Scope) {
		scope.Transaction()
		scope.SetTag("fname", "First Name")

		SendErrorWithRouter(errors.New("my ok version2"))
	})
}
