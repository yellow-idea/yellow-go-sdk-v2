package set_password

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	List(payload *IRequestListPagination) (error, []*ITable, int32)
	Show(id string) (error, *ITable)
	Create(payload *ITable) (error, string, interface{})
	Import(payload []*ITable) (error, string, interface{})
	Update(id string) (error, string, interface{})
	UpdateAction(id string) (error, string, interface{})
	UpdateActionDelete(id string) (error, string, interface{})
	UpdateActionDeleteAll(lineUserID string) (error, string)
	Delete(id string) (error, string, interface{})
	ToggleActive(id string, payload *ITable) (error, string, interface{})
	AppUpdate(id string, payload *ITable) (error, string, interface{})
	TransactionSegment() (error, []*ITable)
	TransactionSegmentUpdate() (error, []*ITable)
	TransactionSegmentDelete() (error, []*ITable)
}

type service struct {
	Repository Repository
}

func NewService(db *mongo.Database) Service {
	return &service{
		Repository: NewRepository(db),
	}
}

func (d *service) List(payload *IRequestListPagination) (error, []*ITable, int32) {
	// region List
	err1, rows := d.Repository.List()
	if err1 != nil {
		return err1, nil, 0
	}
	// endregion

	// region Total
	err2, total := d.Repository.ListTotal(payload)
	if err2 != nil {
		return err2, nil, 0
	}
	// endregion

	return nil, rows, total
}

func (d *service) TransactionSegment() (error, []*ITable) {
	// region List
	err1, rows := d.Repository.TransactionSegment()
	if err1 != nil {
		return err1, rows
	}
	return err1, rows
}

func (d *service) TransactionSegmentUpdate() (error, []*ITable) {
	// region List
	err1, rows := d.Repository.TransactionSegmentUpdate()
	if err1 != nil {
		return err1, rows
	}
	return err1, rows
}

func (d *service) TransactionSegmentDelete() (error, []*ITable) {
	// region List
	err1, rows := d.Repository.TransactionSegmentDelete()
	if err1 != nil {
		return err1, rows
	}
	return err1, rows
}

func (d *service) Show(id string) (error, *ITable) {
	// region Valid & Convert ObjectID
	_id := id

	return d.Repository.Show(_id)
}

func (d *service) Create(payload *ITable) (error, string, interface{}) {
	payload.ID = primitive.NewObjectID()
	payload.CreatedAt = time.Now()
	payload.UpdatedAt = payload.CreatedAt

	err, message, newID := d.Repository.Create(payload)

	return err, message, newID
}

func (d *service) Import(payload []*ITable) (error, string, interface{}) {
	var data []interface{}
	for _, item := range payload {
		item.ID = primitive.NewObjectID()
		//item.IsActive = false
		item.CreatedAt = time.Now()
		item.UpdatedAt = item.CreatedAt
		data = append(data, item)
	}
	return d.Repository.Import(data)
}

func (d *service) Update(id string) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	//payload.UpdatedAt = time.Now()

	return d.Repository.Update(_id)
}

func (d *service) UpdateAction(id string) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	//payload.UpdatedAt = time.Now()

	return d.Repository.UpdateAction(_id)
}

func (d *service) UpdateActionDelete(id string) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	//payload.UpdatedAt = time.Now()

	return d.Repository.UpdateActionDelete(_id)
}

func (d *service) UpdateActionDeleteAll(lineUserID string) (error, string) {
	// region Valid & Convert ObjectID
	// endregion

	//payload.UpdatedAt = time.Now()

	return d.Repository.RemoveByLineUserID(lineUserID)
}
func (d *service) Delete(id string) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	return d.Repository.Delete(_id)
}

func (d *service) ToggleActive(id string, payload *ITable) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	return d.Repository.ToggleActive(_id, payload)
}

func (d *service) AppUpdate(id string, payload *ITable) (error, string, interface{}) {
	// region Valid & Convert ObjectID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err, "BAD_REQUEST", nil
	}
	// endregion

	payload.UpdatedAt = time.Now()

	return d.Repository.Update(_id)
}
