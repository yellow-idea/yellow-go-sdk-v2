package set_password

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ITableConditionGroup struct {
	ID          string `json:"id" bson:"id"`
	IsCondition string `json:"is_condition" bson:"is_condition"`
}

type ITableCondition struct {
	ID           string                   `json:"id" bson:"id"`
	ParentID     string                   `json:"parent_id" bson:"parent_id"`
	IsCondition  string                   `json:"is_condition" bson:"is_condition"`
	Title        string                   `json:"title" bson:"title"`
	SubTitle1    string                   `json:"sub_title1" bson:"sub_title1"`
	QuestionList []map[string]interface{} `json:"questionList" bson:"questionList"`
}

type ITableSubCondition struct {
	ID         string                   `json:"id" bson:"id"`
	ParentID   string                   `json:"parent_id" bson:"parent_id"`
	GroupID    string                   `json:"group_id" bson:"group_id"`
	Source1    string                   `json:"source1" bson:"source1"`
	Source2    string                   `json:"source2" bson:"source2"`
	Operator   string                   `json:"operator" bson:"operator"`
	Value1     []string                 `json:"value1" bson:"value1"`
	Value2     []string                 `json:"value2" bson:"value2"`
	Layer2     []map[string]interface{} `json:"layers2" bson:"layers2"`       // TODO: ชั่วคราว
	Layer3     []map[string]interface{} `json:"layers3" bson:"layers3"`       // TODO: ชั่วคราว
	ChoiceList []map[string]interface{} `json:"choiceList" bson:"choiceList"` // TODO: ชั่วคราว
}

type ITable struct {
	ID             			primitive.ObjectID      `json:"id" bson:"_id"`
	LineUserId           	string                  `json:"mid" bson:"mid"`
	CustomerId      	string      			`json:"customer_id" bson:"customer_id"`
	Department   			string 					`json:"department" bson:"department"`
	Gender       		string                  `json:"gender" bson:"gender"`
	Firstname       		string                  `json:"firstname" bson:"firstname"`
	Lastname       			string                `json:"lastname" bson:"lastname"`
	Status           	string                  `json:"status" bson:"status"`
	Password 		string 					`json:"password" bson:"password"`
	Passcode1      	string      			`json:"passcode1" bson:"passcode1"`
	Passcode2   			string 					`json:"passcode2" bson:"passcode2"`
	EmployeeId       		string                  `json:"employeeId" bson:"employeeId"`
	FullName       		string                  `json:"fullName" bson:"fullName"`
	Position       			string                `json:"position" bson:"position"`
	Birthday       			string                `json:"birthday" bson:"birthday"`
	Age       			string                `json:"age" bson:"age"`
	CreatedAt      			time.Time               `json:"created_at" bson:"created_at"`
	UpdatedAt      			time.Time               `json:"updated_at" bson:"updated_at"`
	DeletedAt      			time.Time               `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
}



type IRequestListPaginationFilter struct {
	Name      string    `json:"name" bson:"name"`
	IsActive  string    `json:"is_active" bson:"is_active"`
	StartDate time.Time `json:"start_date" bson:"start_date"`
	EndDate   time.Time `json:"end_date" bson:"end_date"`
}

type IRequestListPagination struct {
	Sort     string                        `json:"sort" bson:"sort"`
	Order    string                        `json:"order" bson:"order"`
	Offset   int64                         `json:"offset" bson:"offset"`
	Limit    int64                         `json:"limit" bson:"limit"`
	IsExport bool                          `json:"is_export" bson:"is_export"`
	Filter   *IRequestListPaginationFilter `json:"filter" bson:"filter"`
}
