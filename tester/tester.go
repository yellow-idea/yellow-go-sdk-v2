package tester

import (
	"go.mongodb.org/mongo-driver/mongo"
	"testing"
	yellowEncrypt "yellow-sdk/encrypt"
	yellowMongo "yellow-sdk/mongodb"
	yellowViper "yellow-sdk/viper"
)

func MongodbFormENV(path string, fileName string, callback func(db *mongo.Database, err error)) {
	yellowViper.YamlToEnv(path, fileName)
	yellowMongo.MongoCallbackConnect(
		yellowEncrypt.KeyDragonGetENV("DB_PROTO", false),
		yellowEncrypt.KeyDragonGetENV("DB_HOST", false),
		yellowEncrypt.KeyDragonGetENV("DB_USER", false),
		yellowEncrypt.KeyDragonGetENV("DB_PASSWORD", false),
		yellowEncrypt.KeyDragonGetENV("DB_OPTIONS", false),
		yellowEncrypt.KeyDragonGetENV("DB_DATABASE", false),
		callback,
	)
}

func GeneralUtilityServerlessCustom(path string) {
	yellowViper.YamlToEnv(path, "serverless.env.yml")
}

func GeneralUtilityServerless() {
	yellowViper.YamlToEnv("../../", "serverless.env.yml")
}

func MongoServerlessModulesRepositorySDK(callback func(db *mongo.Database)) {
	var t *testing.T
	MongodbFormENV(
		"../",
		"serverless.env.yml", func(db *mongo.Database, err error) {
			if err != nil {
				t.Error(err)
			}
			callback(db)
		})
}

func MongoServerlessModulesRepository(callback func(db *mongo.Database)) {
	var t *testing.T
	MongodbFormENV(
		"../../../",
		"serverless.env.yml", func(db *mongo.Database, err error) {
			if err != nil {
				t.Error(err)
			}
			callback(db)
		})
}

func MongoServerlessModulesServiceSDK(callback func(db *mongo.Database)) {
	var t *testing.T
	MongodbFormENV(
		"../",
		"serverless.env.yml", func(db *mongo.Database, err error) {
			if err != nil {
				t.Error(err)
			}
			callback(db)
		})
}

func MongoServerlessModulesService(callback func(db *mongo.Database)) {
	var t *testing.T
	MongodbFormENV(
		"../../../",
		"serverless.env.yml", func(db *mongo.Database, err error) {
			if err != nil {
				t.Error(err)
			}
			callback(db)
		})
}

func MongoServerlessModulesService2(path string,callback func(db *mongo.Database)) {
	var t *testing.T
	MongodbFormENV(
		path,
		"serverless.env.yml", func(db *mongo.Database, err error) {
			if err != nil {
				t.Error(err)
			}
			callback(db)
		})
}
