package time

import (
	"time"
)

func GetTimeNowGMT() time.Time {
	return time.Now().UTC().Add(7 * time.Hour)
	//return GetTimeNowBangkok()
}

func GetTimeFormat1(t time.Time) string {
	return t.Format("2006-01-02 15:04")
}

func GetTimeNowBangkok() time.Time {
	//loc, _ := time.LoadLocation("Asia/Bangkok")
	//return time.Now().In(loc)
	return GetTimeNowGMT()
}

func TimeToDayThai(t time.Time) string {
	d := t.Format("Monday")
	switch d {
	case "Monday":
		return "จันทร์"
	case "Tuesday":
		return "อังคาร"
	case "Wednesday":
		return "พุธ"
	case "Thursday":
		return "พฤหัส"
	case "Friday":
		return "ศุกร์"
	case "Saturday":
		return "เสาร์"
	case "Sunday":
		return "อาทิตย์"
	}
	return ""
}

func GetTimeNowDayThai() string {
	return TimeToDayThai(GetTimeNowBangkok())
}