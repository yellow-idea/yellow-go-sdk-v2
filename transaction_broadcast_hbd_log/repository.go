package transaction_broadcast_hbd_log

import (
	yellowMongo "yellow-sdk/mongodb"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Repository interface {
	List() (error, []*ITable)
	ListTotal(payload *IRequestListPagination) (error, int32)
	Create(payload *ITable) (error, string, interface{})
}

type repository struct {
	CollectionName string
	DB             *mongo.Database
	Collection     *yellowMongo.MongoCollection
}

func NewRepository(db *mongo.Database) Repository {
	return &repository{
		CollectionName: CollectionName,
		DB:             db,
		Collection:     yellowMongo.NewCollection(db, CollectionName),
	}
}

func (d *repository) getListFilter() bson.D {
	filter := bson.D{}

	//filter = append(filter, bson.E{Key: "is_pull", Value: false})
	filter = append(filter, bson.E{
		Key:   "is_pull",
		Value: bson.D{{"$ne", true}},
	})
	//filter = append(filter, bson.E{Key: "deleted_at", Value: nil})
	//if payload.Filter.Name != "" {
	//	filter = append(filter, bson.E{
	//		Key:   "name",
	//		Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.Name)}},
	//	})
	//}
	//if payload.Filter.IsActive == "1" {
	//	filter = append(filter, bson.E{Key: "is_active", Value: true})
	//} else if payload.Filter.IsActive == "0" {
	//	filter = append(filter, bson.E{Key: "is_active", Value: false})
	//}
	//if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
	//	filter = append(filter, bson.E{
	//		Key: "created_at",
	//		Value: bson.M{
	//			"$gte": payload.Filter.StartDate,
	//			"$lte": payload.Filter.EndDate,
	//		}})
	//}
	return filter
}

func (d *repository) List() (error, []*ITable) {
	//orderBy := -1
	//sort := "updated_at"
	//if payload.Sort != "" {
	//	sort = payload.Sort
	//}
	//if payload.Order == "desc" {
	//	orderBy = -1
	//}

	findOptions := options.Find()
	//findOptions.SetSort(&bson.D{{sort, orderBy}})
	//findOptions.SetSkip(payload.Offset)
	//findOptions.SetLimit(payload.Limit)

	filter := d.getListFilter()

	rows := make([]*ITable, 0)
	err := yellowMongo.MongoExecutorFind(d.DB.Collection(d.CollectionName), filter, findOptions,
		func(cur *mongo.Cursor) {
			var item *ITable
			_ = cur.Decode(&item)
			rows = append(rows, item)
		})
	defer func() { rows = make([]*ITable, 0) }()
	return err, rows
}

func (d *repository) ListTotal(payload *IRequestListPagination) (error, int32) {
	return d.Collection.CountDocuments(d.getListFilter())
}

func (d *repository) Create(payload *ITable) (error, string, interface{}) {
	return d.Collection.InsertOne(payload)
}
