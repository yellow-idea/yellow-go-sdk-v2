package transaction_broadcast_log

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	List(payload *IRequestListPagination) (error, []*ITable, int32)
	Create(payload *ITable) (error, string, interface{})

}

type service struct {
	Repository Repository
}

func NewService(db *mongo.Database) Service {
	return &service{
		Repository: NewRepository(db),
	}
}

func (d *service) List(payload *IRequestListPagination) (error, []*ITable, int32) {
	// region List
	err1, rows := d.Repository.List(payload)
	if err1 != nil {
		return err1, nil, 0
	}
	// endregion

	// region Total
	err2, total := d.Repository.ListTotal(payload)
	if err2 != nil {
		return err2, nil, 0
	}
	// endregion

	return nil, rows, total
}

func (d *service) Create(payload *ITable) (error, string, interface{}) {
	payload.ID = primitive.NewObjectID()
	payload.CreatedAt = time.Now()
	err, message, newID := d.Repository.Create(payload)

	return err, message, newID
}

