package transaction_follow

import (
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	yellowMongo "yellow-sdk/mongodb"
)

type Repository interface {
	List(mid string) (error, []*ITable)
	Show(mid string) (error, *ITable)
}

type repository struct {
	CollectionName string
	DB             *mongo.Database
	Collection     *yellowMongo.MongoCollection
}

func NewRepository(db *mongo.Database) Repository {
	return &repository{
		CollectionName: CollectionName,
		DB:             db,
		Collection:     yellowMongo.NewCollection(db, CollectionName),
	}
}

func (d *repository) getListFilter(mid string) bson.D {
	filter := bson.D{}
	if mid != "" {
		filter = append(filter, bson.E{
			Key:   "mid",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", mid)}},
		})
	}
	return filter
}

func (d *repository) List(mid string) (error, []*ITable) {
	orderBy := -1
	sort := "created_at"

	findOptions := options.Find()
	findOptions.SetSort(&bson.D{{sort, orderBy}})
	filter := d.getListFilter(mid)

	rows := make([]*ITable, 0)
	err := yellowMongo.MongoExecutorFind(d.DB.Collection(d.CollectionName), filter, findOptions,
		func(cur *mongo.Cursor) {
			var item *ITable
			_ = cur.Decode(&item)
			rows = append(rows, item)
		})
	defer func() { rows = make([]*ITable, 0) }()
	return err, rows
}


func (d *repository) Show(mid string) (error, *ITable) {
	var item *ITable
	err := d.Collection.FindOne(bson.D{{"_id", mid}}).Decode(&item)
	if err != nil {
		if err.Error() != "mongo: no documents in result" {
			return err, item
		}
		return nil, nil
	}
	return nil, item
}
