package transaction_follow

import (
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	List(mid string) (error, []*ITable)
	Show(mid string) (error, *ITable)
}

type service struct {
	Repository Repository
}

func NewService(db *mongo.Database) Service {
	return &service{
		Repository: NewRepository(db),
	}
}

func (d *service) List(mid string) (error, []*ITable) {
	// region List
	err1, rows := d.Repository.List(mid)
	if err1 != nil {
		return err1, nil
	}
	// endregion


	return nil, rows
}

func (d *service) Show(_id string) (error, *ITable) {

	return d.Repository.Show(_id)
}
