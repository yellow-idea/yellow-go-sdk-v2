package transaction_follow

import (
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"testing"
	yellowTester "yellow-sdk/tester"
)

func TestServiceCreate(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewService(db)
		payload := getPayloadMock()
		err, message, _id := sv.Create(payload)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(_id, message)
	}
	yellowTester.MongoServerlessModulesServiceSDK(fn)
}

func TestServiceUpdate(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewService(db)
		// region Mock
		idString := getIDStringMock()
		_id := getIDMock()
		payload := getPayloadMock()
		payload.ID = _id
		// endregion
		err, message, id := sv.Update(idString, payload)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(id, message)
	}
	yellowTester.MongoServerlessModulesServiceSDK(fn)
}

func TestServiceList(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewService(db)
		payload := getPayloadPaginationMock()
		err, rows, total := sv.List(payload)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(total, rows)
	}
	yellowTester.MongoServerlessModulesServiceSDK(fn)
}

func TestServiceShow(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewService(db)
		idString := getIDStringMock()
		err, data := sv.Show(idString)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(data)
	}
	yellowTester.MongoServerlessModulesServiceSDK(fn)
}

func TestServiceDelete(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewService(db)
		idString := getIDStringMock()
		err, message, id := sv.Delete(idString)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(id, message)
	}
	yellowTester.MongoServerlessModulesServiceSDK(fn)
}

func TestServiceToggleActive(t *testing.T) {
	fn := func(db *mongo.Database) {
		sv := NewService(db)
		idString := getIDStringMock()
		payload := getPayloadMock()
		err, message, id := sv.ToggleActive(idString, payload)
		if err != nil {
			t.Error(err)
		}
		fmt.Println(id, message)
	}
	yellowTester.MongoServerlessModulesServiceSDK(fn)
}