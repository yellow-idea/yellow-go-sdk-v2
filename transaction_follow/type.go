package transaction_follow

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ITable struct {
	ID        primitive.ObjectID `json:"id" bson:"_id"`
	Mid      string             `json:"mid" bson:"mid"`
	Type     string     		`json:"type" bson:"type"`
	CreatedAt time.Time          `json:"created_at" bson:"created_at"`
}

type IRequestListPaginationFilter struct {
	Mid      string    `json:"mid" bson:"mid"`
}

type IRequestListPagination struct {
	Sort     string                        `json:"sort" bson:"sort"`
	Order    string                        `json:"order" bson:"order"`
	Offset   int64                         `json:"offset" bson:"offset"`
	Limit    int64                         `json:"limit" bson:"limit"`
	IsExport bool                          `json:"is_export" bson:"is_export"`
	Filter   *IRequestListPaginationFilter `json:"filter" bson:"filter"`
}