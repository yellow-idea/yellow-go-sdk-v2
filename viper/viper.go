package viper

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"reflect"
	"strconv"
	"strings"
)

func YamlToEnv(path string, fileName string) {
	viper.SetConfigType("yaml")
	viper.AddConfigPath(path)
	viper.SetConfigName(fileName)
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println(err)
	}
	a := viper.Get("dev").(map[string]interface{})
	for i, v := range a {
		key := strings.ToUpper(i)
		if reflect.TypeOf(v).Kind() == reflect.Int {
			s := strconv.Itoa(v.(int))
			_ = os.Setenv(key, s)
		} else {
			_ = os.Setenv(key, v.(string))
		}
	}
}
